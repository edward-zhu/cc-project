module org.rikarika.cc.Pr3
{

/* space matching */
space [ \n\t]
    | "/*" ([^*]|[*][*]*[^*/])*[*]+[/] // match multiline comment w/o nesting
    | "//" .*; // match single line comment

/* lexical analysis */
token INT   | ⟨DIGIT⟩+ ;
token ID    | [a-zA-Z$_][a-zA-Z0-9$_]* ;
token STRING | \"([^\"\\]|[\\]([nt\"\\]|(x⟨HEX⟩⟨HEX⟩)|(⟨OCT⟩⟨OCT⟩?⟨OCT⟩?)|\n))*\";
// token MAINFUNCID | "function int main"; // main function matching (described in doc.)

token fragment DIGIT | [0-9] ;
token fragment HEX | [0-9a-fA-F] ;
token fragment OCT | [0-7] ;

/* syntatic analysis */

/* expressions combination */
sort MoreExp
    | ⟦,⟨Exp⟩⟨MoreExp⟩⟧
    | ⟦⟧
    ;

/* expression (in precedence order) */
sort Exp
    /* l-value */
    | ⟦⟨ID⟩⟧@10      // identifer
    | ⟦*⟨Exp@9⟩⟧@10    // derefer

    | sugar ⟦(⟨Exp#⟩)⟧@9 → Exp#
    | ⟦⟨STRING⟩⟧@8  // string
    | ⟦⟨INT⟩⟧@8     // Integer

    /* unary opers */
    | ⟦null(⟨Type⟩)⟧@8      // null
    | ⟦sizeof(⟨Type⟩)⟧@8    // sizeof
    | ⟦!⟨Exp⟩⟧@7    // logical negative
    | ⟦-⟨Exp⟩⟧@7    // negative
    | ⟦+⟨Exp⟩⟧@7    // plus
    | ⟦&⟨Exp⟩⟧@7    // reference

    | ⟦⟨Exp@9⟩()⟧@8
    | ⟦⟨Exp@9⟩( ⟨Exp@0⟩⟨MoreExp⟩ )⟧@8

    /* binary opers - arithmetic */
    | ⟦⟨Exp@6⟩*⟨Exp@7⟩⟧@6 // multiply, divide\
    | ⟦⟨Exp@6⟩/⟨Exp@7⟩⟧@6
    | ⟦⟨Exp@6⟩%⟨Exp@7⟩⟧@6 // modulo
    | ⟦⟨Exp@5⟩+⟨Exp@6⟩⟧@5 // plus, minus
    | ⟦⟨Exp@5⟩-⟨Exp@6⟩⟧@5 

    /* binary opers - comparison */
    | ⟦⟨Exp@4⟩<⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩<=⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩>⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩>=⟨Exp@5⟩⟧@4

    /* binary opers - logical */
    | ⟦⟨Exp@3⟩!=⟨Exp@4⟩⟧@3 // neq
    | ⟦⟨Exp@3⟩==⟨Exp@4⟩⟧@3 // eq
    | ⟦⟨Exp@2⟩&&⟨Exp@3⟩⟧@2 // and
    | ⟦⟨Exp@1⟩||⟨Exp@2⟩⟧@1 // or
    ;

sort MoreType
    | ⟦ , ⟨Type⟩⟨MoreType⟩ ⟧
    | ⟦⟧
    ;

sort Type
    | ⟦ int ⟧@2
    | ⟦ char ⟧@2
    | ⟦ *⟨Type@2⟩ ⟧@2
    | sugar ⟦ (⟨Type#⟩) ⟧@2 → Type#
    | ⟦ ⟨Type@2⟩ ( )⟧@1
    | ⟦ ⟨Type@2⟩ (⟨Type⟩⟨MoreType⟩)⟧@1
    ;


sort MoreStmt
    | ⟦⟨Stmt⟩⟨MoreStmt⟩⟧
    | ⟦⟧
    ;

sort Stmt
    | ⟦var⟨Type⟩⟨ID⟩ ;⟧
    | ⟦⟨Exp@10⟩ = ⟨Exp⟩ ;⟧
    | ⟦if (⟨Exp⟩) ⟨Stmt⟩⟧
    | ⟦if (⟨Exp⟩) ⟨Stmt⟩ else ⟨Stmt⟩⟧
    | ⟦while (⟨Exp⟩) ⟨Stmt⟩ ⟧
    | ⟦return ⟨Exp⟩ ;⟧
    | ⟦{⟨MoreStmt⟩}⟧
    ;

sort TypeIds
    | ⟦,⟨Type⟩ ⟨ID⟩ ⟨TypeIds⟩⟧
    | ⟦⟧
    ;

sort ParamDecl
    | ⟦(⟨Type⟩ ⟨ID⟩ ⟨TypeIds⟩)⟧
    | ⟦()⟧
    ;

sort FuncDecl
    | ⟦function ⟨Type⟩ ⟨ID⟩ ⟨ParamDecl⟩ { ⟨MoreStmt⟩ }⟧
    ;

sort FuncDecls
    | ⟦⟨FuncDecl⟩⟨FuncDecls⟩⟧
    | ⟦⟨FuncDecl⟩⟧
    ;

main sort Prog
    | ⟦⟨FuncDecls⟩⟧
    ;

/***
    Semantic Analysis
***/

/* Type Check Functions */

sort Type | scheme TCToInt(Type);
TCToInt(⟦int⟧) → ⟦int⟧;
TCToInt(⟦* ⟨Type#⟩⟧) → ⟦int⟧;
default TCToInt(#) → error ⟦must be a pointer or int value⟧;

sort Type | scheme TCPToInt(Type, Type);
TCPToInt(⟦int⟧, ⟦int⟧) → ⟦int⟧;

sort Type | scheme TCDeRef(Type);
TCDeRef(⟦* ⟨Type#⟩⟧) → #;

sort Type | scheme TCPlusMinus(Type, Type);
TCPlusMinus(⟦*⟨Type#⟩⟧, ⟦int⟧) → ⟦*⟨Type#⟩⟧;
TCPlusMinus(⟦int⟧, ⟦int⟧) → ⟦int⟧;
default TCPlusMinus(#1, #2) → error ⟦type mismatch⟧;

sort Type | scheme TCEq(Type, Type);
TCEq(⟦int⟧, ⟦int⟧) → ⟦int⟧;
TCEq(⟦* ⟨Type#⟩⟧, ⟦* ⟨Type#⟩⟧) → ⟦ int ⟧;
default TCEq(#1, #2) → error ⟦type mismatch⟧;

sort Type | scheme TCComp(Type, Type);
TCComp(⟦int⟧, ⟦int⟧) → ⟦int⟧;
default TCComp(#1, #2) → error ⟦comparison oper can only take two int oprands.⟧;

sort Type | scheme TCLogical(Type, Type);
TCLogical(#1, #2) → TCPToInt(TCToInt(#1), TCToInt(#2));

sort Type | scheme Equal(Type, Type);
Equal(#, #) → #;
Equal(⟦*⟨Type#1⟩⟧, ⟦*⟨Type#2⟩⟧) → ⟦* ⟨Type Equal(#1, #2)⟩⟧;
default Equal(#1, #2) → error ⟦inequal type⟧;

attribute ↑t(Type);

sort Type | scheme GetType(Exp);
GetType(⟦⟨Exp#⟩⟧) → GetType2(⟦⟨Exp Ee(#)⟩⟧);
{
    | scheme GetType2(Exp);
    GetType2(⟦⟨Exp# ↑t(#t)⟩⟧) → #t;
}

sort Type | scheme Equal3(Type, Type, Type);
Equal3(#1, #2, #2) → #1;
default Equal3(#1, #2, #3) → error ⟦type mismatch 3⟧;

sort Type | scheme Equal3a(Type, MoreType, MoreExp);
Equal3a(#1, ⟦,⟨Type#t1⟩⟨MoreType#mt⟩⟧, ⟦,⟨Exp#e ↑t(#t1)⟩⟨MoreExp#me⟩⟧) → Equal3a(#1, #mt, #me);
Equal3a(#1, ⟦⟧, ⟦⟧) → #1;
default Equal3a(#1, #2, #3) → error ⟦type mismatch 3a⟧;

sort Type | scheme Equal5(Type, Type, Type, MoreType, MoreExp);
Equal5(#1, #2, #2, #mt, #me) → Equal3a(#1, #mt, #me);
default Equal5(#1, #2, #3, #4, #5) → error ⟦type mismatch 5⟧;

/* Type Check - Expression */

sort Exp | ↑t;
⟦⟨INT#⟩⟧↑t(⟦int⟧);
⟦⟨STRING#⟩⟧↑t(⟦* char⟧);
⟦null(⟨Type#⟩)⟧↑t(#);
⟦sizeof(⟨Type#⟩)⟧↑t(⟦int⟧);
⟦* ⟨Exp# ↑t(#t)⟩⟧↑t(TCDeRef(#t));
⟦& ⟨Exp# ↑t(#t)⟩⟧↑t(⟦* ⟨Type#t⟩⟧);
⟦⟨Exp#1 ↑t(⟦⟨Type#t⟩( )⟧)⟩()⟧↑t(#t);
⟦⟨Exp#1 ↑t(⟦⟨Type#t⟩(⟨Type#t1⟩⟨MoreType#mt⟩)⟧)⟩ (⟨Exp#2 ↑t(#t2)⟩⟨MoreExp#me⟩)⟧↑t(Equal5(#t, #t1, #t2, #mt, #me));

// arithmatic op
⟦(⟨Exp#1 ↑t(#t1)⟩ + ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(TCPlusMinus(#t1, #t2));
⟦(⟨Exp#1 ↑t(#t1)⟩ * ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(Equal(#t1, #t2));

// equality op
⟦(⟨Exp#1 ↑t(#t1)⟩ == ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(TCEq(#t1, #t2));

// comparison op
⟦(⟨Exp#1 ↑t(#t1)⟩ < ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(TCComp(#t1, #t2));

// logical op
⟦(⟨Exp#1 ↑t(#t1)⟩ && ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(TCLogical(#t1, #t2));


attribute ↓e{ID : Type};

sort MoreExp | scheme MEe(MoreExp) ↓e;
MEe(⟦, ⟨Exp#1⟩⟨MoreExp#me⟩⟧)↓e{:#MEe} → ⟦, ⟨Exp Ee(#1) ↓e{:#MEe}⟩⟨MoreExp MEe(#me) ↓e{:#MEe}⟩⟧;
MEe(⟦⟧)↓e{:#MEe} → ⟦⟧;

sort Exp | scheme Ee(Exp) ↓e;
Ee(⟦⟨ID#v⟩⟧)↓e{#v: #t} → ⟦⟨ID#v⟩⟧ ↑t(#t);
Ee(⟦⟨ID#v⟩⟧)↓e{¬#v} → error ⟦undefined id⟧;
Ee(⟦⟨STRING#1⟩⟧) → ⟦⟨STRING#1⟩⟧↑t(⟦* char⟧);
Ee(⟦⟨INT#1⟩⟧) → ⟦⟨INT#1⟩⟧↑t(⟦int⟧);
Ee(⟦sizeof(⟨Type#⟩)⟧) → ⟦sizeof(⟨Type#⟩)⟧ ↑t(⟦int⟧);
Ee(⟦null(⟨Type#⟩)⟧) → ⟦null(⟨Type#⟩)⟧ ↑t(#);
Ee(⟦*⟨Exp#1⟩⟧)↓e{:#Ee} → ⟦*⟨Exp Ee(#1)↓e{:#Ee}⟩⟧;
Ee(⟦&⟨Exp#1⟩⟧)↓e{:#Ee} → ⟦&⟨Exp Ee(#1)↓e{:#Ee}⟩⟧;
Ee(⟦⟨Exp#1⟩()⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩()⟧;
Ee(⟦⟨Exp#1⟩(⟨Exp#2⟩⟨MoreExp#3⟩)⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩(⟨Exp Ee(#2)↓e{:#Ee}⟩⟨MoreExp#3⟩)⟧;

// arithmatic op
Ee(⟦⟨Exp#1⟩ + ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ + ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;
Ee(⟦⟨Exp#1⟩ * ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ * ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;

// equality op
Ee(⟦⟨Exp#1⟩ == ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ == ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;

// comparison op
Ee(⟦⟨Exp#1⟩ < ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ < ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;

// logical op
Ee(⟦⟨Exp#1⟩ && ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ && ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;

default Ee(#) → error ⟦invalid expression⟧;

/* Type Check - Statements */

sort MoreStmt | scheme Se2(MoreStmt) ↓e;
Se2(⟦var ⟨Type#t⟩⟨ID#v⟩; ⟨MoreStmt#1⟩⟧)↓e{:#Se2} →
    ⟦var ⟨Type#t⟩⟨ID#v⟩; ⟨MoreStmt Se2(#1)↓e{:#Se2} ↓e{#v:#t}⟩⟧;
Se2(⟦⟨Stmt#1⟩⟨MoreStmt#2⟩⟧)↓e{:#Se2}
    → ⟦⟨Stmt Se(#1)↓e{:#Se2}⟩ ⟨MoreStmt Se2(#2) ↓e{:#Se2}⟩⟧;
Se2(⟦⟧) → ⟦⟧;


sort Stmt | scheme Se(Stmt) ↓e;
Se(⟦⟨Exp#1⟩=⟨Exp#2⟩;⟧)↓e{:#Se}
    → SeB(⟦⟨Exp Ee(#1)↓e{:#Se}⟩= ⟨Exp Ee(#2)↓e{:#Se}⟩;⟧);
{
    | scheme SeB(Stmt);
    SeB(⟦⟨Exp#1 ↑t(#t1)⟩=⟨Exp#2 ↑t(#t2)⟩;⟧) →
        ⟦⟨Exp#1⟩ = ⟨Exp#2⟩;⟧↑t(Equal(#t1, #t2));
}
Se(⟦if(⟨Exp#1⟩) ⟨Stmt#s⟩⟧)↓e{:#Se} → SeIf(⟦if (⟨Exp Ee(#1) ↓e{:#Se}⟩) ⟨Stmt Se(#s) ↓e{:#Se}⟩⟧);
{
    | scheme SeIf(Stmt);
    SeIf(⟦if (⟨Exp#1 ↑t(#t)⟩) ⟨Stmt#s⟩⟧) → ⟦if (⟨Exp#1⟩) ⟨Stmt#s⟩⟧ ↑t(TCToInt(#t));
}
Se(⟦if(⟨Exp#1⟩) ⟨Stmt#s1⟩ else ⟨Stmt#s2⟩⟧) ↓e{:#Se} → SeIfElse(⟦if(⟨Exp Ee(#1)↓e{:#Se}⟩)
    ⟨Stmt Se(#s1) ↓e{:#Se}⟩ else ⟨Stmt Se(#s2) ↓e{:#Se}⟩⟧);
{
    | scheme SeIfElse(Stmt);
    SeIfElse(⟦if (⟨Exp#1 ↑t(#t)⟩) ⟨Stmt#s1⟩ else ⟨Stmt#s2⟩⟧) → ⟦if (⟨Exp#1⟩) ⟨Stmt#s1⟩ else ⟨Stmt#s2⟩⟧ ↑t(TCToInt(#t));
}
Se(⟦while (⟨Exp#1⟩) ⟨Stmt#s⟩⟧)↓e{:#Se} → SeWhile(⟦while (⟨Exp Ee(#1)↓e{:#Se}⟩) ⟨Stmt Se(#s) ↓e{:#Se}⟩⟧);
{
    | scheme SeWhile(Stmt);
    SeWhile(⟦while (⟨Exp#1 ↑t(#t)⟩) ⟨Stmt#s⟩⟧) → ⟦while (⟨Exp#1⟩) ⟨Stmt#s⟩⟧↑t(TCToInt(#t));
}
Se(⟦return ⟨Exp#1⟩ ;⟧)↓e{:#Se} → SeReturn(⟦return ⟨Exp Ee(#1) ↓e{:#Se}⟩;⟧) ↓e{:#Se};
{
    | scheme SeReturn(Stmt) ↓e;
    SeReturn(⟦return ⟨Exp#1 ↑t(#t)⟩ ;⟧)↓e{⟦ret⟧:#rt} → ⟦return ⟨Exp#1⟩ ;⟧↑t(Equal(#t, #rt));
    SeReturn(#)↓e{¬⟦ret⟧} → error ⟦internal error⟧;
}
Se(⟦{⟨MoreStmt#⟩}⟧)↓e{:#Se} → ⟦{⟨MoreStmt Se2(#) ↓e{:#Se}⟩}⟧;

attribute ↑d{ID : Type};

sort ParamDecl | ↑d;
⟦(⟨Type#t⟩ ⟨ID#v⟩ ⟨TypeIds#ts ↑d{:#d}⟩)⟧ ↑d{:#d} ↑d{#v:#t};
⟦()⟧↑d{};

sort TypeIds | ↑d;
⟦, ⟨Type#t⟩ ⟨ID#v⟩ ⟨TypeIds#ts ↑d{:#d}⟩⟧ ↑d{:#d} ↑d{#v:#t};
⟦⟧↑d{};


attribute ↓pt(Type);
attribute ↑mt(MoreType);

sort TypeIds | ↑mt;
⟦, ⟨Type#t⟩ ⟨ID#v⟩ ⟨TypeIds#ts ↑mt(#mt)⟩⟧ ↑mt(⟦, ⟨Type#t⟩⟨MoreType#mt⟩⟧);
⟦⟧↑mt(⟦⟧);

/* Type Check - Functions */

sort FuncDecl | scheme Fe(FuncDecl) ↓e;
Fe(⟦function ⟨Type#t⟩ ⟨ID#n⟩ ⟨ParamDecl#pd⟩ {⟨MoreStmt#s⟩}⟧) ↓e{#n:#nt} → error ⟦duplicate function name⟧;
Fe(⟦function ⟨Type#t⟩ ⟨ID#n⟩ () { ⟨MoreStmt#s⟩ }⟧) ↓e{:#Fe} →
    ⟦function ⟨Type#t⟩ ⟨ID#n⟩ () {
        ⟨MoreStmt Se2(#s) ↓e{:#Fe} ↓e{#n:⟦⟨Type#t⟩( )⟧} ↓e{⟦ret⟧:⟦⟨Type#t⟩⟧}⟩ }⟧;
Fe(⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩ ⟨TypeIds#ts ↑d{:#d} ↑mt(#mt)⟩) { ⟨MoreStmt#s⟩ }⟧) ↓e{:#Fe} →
    ⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩⟨TypeIds#ts⟩)
        { ⟨MoreStmt Se2(#s) ↓e{#n1:#t1} ↓e{:#Fe} ↓e{:#d}
            ↓e{#n:⟦⟨Type#t⟩(⟨Type#t1⟩⟨MoreType#mt⟩)⟧} ↓e{⟦ret⟧:⟦⟨Type#t⟩⟧}⟩}⟧;

sort FuncDecl | ↑d;
⟦function ⟨Type#t⟩ ⟨ID#n⟩ () { ⟨MoreStmt#s⟩ }⟧ ↑d{#n:⟦⟨Type#t⟩( )⟧};
⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩ ⟨TypeIds#ts ↑mt(#mt)⟩) { ⟨MoreStmt#s⟩ }⟧
    ↑d{#n:⟦⟨Type#t⟩ (⟨Type#t1⟩⟨MoreType#mt⟩)⟧};

sort FuncDecls | scheme Fe2(FuncDecls) ↓e;
Fe2(⟦⟨FuncDecl#fd⟩⟨FuncDecls#fds⟩⟧)↓e{:#Fe2} → Fe2B(⟦⟨FuncDecl Fe(#fd) ↓e{:#Fe2}⟩⟨FuncDecls#fds⟩⟧) ↓e{:#Fe2};
{
    | scheme Fe2B(FuncDecls) ↓e;
    Fe2B(⟦function ⟨Type#t⟩ ⟨ID#n⟩ () { ⟨MoreStmt#s⟩} ⟨FuncDecls#fds⟩⟧) ↓e{:#Fe2}
        → ⟦function ⟨Type#t⟩ ⟨ID#n⟩ () {⟨MoreStmt#s⟩} ⟨FuncDecls Fe2(#fds) ↓e{:#Fe2} ↓e{#n:⟦⟨Type#t⟩( )⟧}⟩⟧;
    Fe2B(⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩ ⟨TypeIds#ts ↑mt(#mt)⟩) { ⟨MoreStmt#s⟩ } ⟨FuncDecls#fds⟩⟧) ↓e{:#Fe2}
        → ⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩ ⟨TypeIds#ts⟩) {⟨MoreStmt#s⟩}
            ⟨FuncDecls Fe2(#fds) ↓e{:#Fe2} ↓e{#n:⟦⟨Type#t⟩ (⟨Type#t1⟩⟨MoreType#mt⟩)⟧}⟩⟧;
}
Fe2(⟦⟨FuncDecl#fd⟩⟧)↓e{:#Fe2} → ⟦⟨FuncDecl Fe(#fd) ↓e{:#Fe2}⟩⟧;

sort FuncDecls | ↑d;
⟦⟨FuncDecl#fd ↑d{:#d}⟩⟨FuncDecls#fds ↑d{:#ds}⟩⟧↑d{:#d} ↑d{:#ds};
⟦⟨FuncDecl#fd ↑d{:#d}⟩⟧↑d{:#d};

sort Prog | scheme TypeCheck(Prog);
TypeCheck(⟦⟨FuncDecls #fds⟩⟧) → MainCheck(⟦⟨FuncDecls Fe2(#fds)
    ↓e{⟦malloc⟧: ⟦* char(int)⟧}
    ↓e{⟦puts⟧: ⟦int(* char)⟧}
    ↓e{⟦puti⟧: ⟦int(int)⟧}
    ↓e{⟦atoi⟧: ⟦int(*char)⟧}
    ↓e{⟦div⟧: ⟦int(int, int)⟧}
    ↓e{⟦mod⟧: ⟦int(int, int)⟧}
    ⟩⟧);
{
    | scheme MainCheck(Prog);
    MainCheck(⟦⟨FuncDecls#fds ↑d{⟦main⟧:#t}⟩⟧) → ⟦⟨FuncDecls#fds⟩⟧↑t(Equal(#t, ⟦int(*char)⟧));
    MainCheck(⟦⟨FuncDecls#fds ↑d{¬⟦main⟧}⟩⟧) → error ⟦no main function.⟧;
}

////////////////////////////////////////////////////////////////////////
// 2. MinARM32 ASSEMBLER GRAMMAR
////////////////////////////////////////////////////////////////////////

  // arm: refers to http://cs.nyu.edu/courses/fall14/CSCI-GA.2130-001/pr3/MinARM32.pdf
 
  // Instructions.
  sort Instructions | ⟦ ⟨Instruction⟩ ⟨Instructions⟩ ⟧ | ⟦⟧ ;

  // Directives (arm:2.1)
  sort Instruction
    | ⟦DEF ⟨ID⟩ = ⟨Integer⟩ ¶⟧  // define identifier
    | ⟦¶⟨Label⟩ ⟧               // define address label
    | ⟦DCI ⟨Integers⟩ ¶⟧        // allocate integers
    | ⟦DCS ⟨String⟩ ¶⟧          // allocate strings
    | ⟦⟨Op⟩ ¶⟧                  // machine instruction
    ;

  sort Integers | ⟦ ⟨Integer⟩, ⟨Integers⟩ ⟧ | ⟦ ⟨Integer⟩ ⟧ ;

  sort Label | symbol ⟦⟨ID⟩⟧ ;
 
  // Syntax of individual machine instructions (arm:2.2).
  sort Op

    | ⟦MOV ⟨Reg⟩, ⟨Arg⟩ ⟧		// move
    | ⟦MVN ⟨Reg⟩, ⟨Arg⟩ ⟧		// move not
    | ⟦ADD ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// add
    | ⟦SUB ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// subtract
    | ⟦RSB ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// reverse subtract
    | ⟦AND ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// bitwise and
    | ⟦ORR ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// bitwise or
    | ⟦EOR ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// bitwise exclusive or
    | ⟦CMP ⟨Reg⟩, ⟨Arg⟩ ⟧	    	// compare
    | ⟦MUL ⟨Reg⟩, ⟨Reg⟩, ⟨Reg⟩ ⟧	// multiply

    | ⟦B ⟨Label⟩ ⟧			// branch always
    | ⟦BEQ ⟨Label⟩ ⟧			// branch if equal
    | ⟦BNE ⟨Label⟩ ⟧			// branch if not equal
    | ⟦BGT ⟨Label⟩ ⟧			// branch if greater than
    | ⟦BLT ⟨Label⟩ ⟧			// branch if less than
    | ⟦BGE ⟨Label⟩ ⟧			// branch if greater than or equal
    | ⟦BLE ⟨Label⟩ ⟧			// branch if less than or equal
    | ⟦BL ⟨Label⟩ ⟧			// branch and link

    | ⟦LDR ⟨Reg⟩, ⟨Mem⟩ ⟧		// load register from memory
    | ⟦STR ⟨Reg⟩, ⟨Mem⟩ ⟧		// store register to memory

    | ⟦LDMFD ⟨Reg⟩! , {⟨Regs⟩} ⟧ 	// load multiple fully descending (pop)
    | ⟦STMFD ⟨Reg⟩! , {⟨Regs⟩} ⟧	// store multiple fully descending (push)
    ;

  // Arguments.

  sort Reg	| ⟦R0⟧ | ⟦R1⟧ | ⟦R2⟧ | ⟦R3⟧ | ⟦R4⟧ | ⟦R5⟧ | ⟦R6⟧ | ⟦R7⟧
		| ⟦R8⟧ | ⟦R9⟧ | ⟦R10⟧ | ⟦R11⟧ | ⟦R12⟧ | ⟦SP⟧ | ⟦LR⟧ | ⟦PC⟧ ;

  sort Arg | ⟦⟨Constant⟩⟧ | ⟦⟨Reg⟩⟧ | ⟦⟨Reg⟩, LSL ⟨Constant⟩⟧ | ⟦⟨Reg⟩, LSR ⟨Constant⟩⟧ ;

  sort Mem | ⟦[⟨Reg⟩, ⟨Sign⟩⟨Arg⟩]⟧ ;
  sort Sign | ⟦+⟧ | ⟦-⟧ | ⟦⟧ ;

  sort Regs | ⟦⟨Reg⟩⟧ | ⟦⟨Reg⟩-⟨Reg⟩⟧ | ⟦⟨Reg⟩, ⟨Regs⟩⟧ | ⟦⟨Reg⟩-⟨Reg⟩, ⟨Regs⟩⟧ ;

  sort Constant | ⟦#⟨Integer⟩⟧ | ⟦&⟨Label⟩⟧ ;

  // Helper concatenation/flattening of Instructions.
  sort Instructions | scheme ⟦ { ⟨Instructions⟩ } ⟨Instructions⟩ ⟧ ;
  ⟦ {} ⟨Instructions#⟩ ⟧ → # ;
  ⟦ {⟨Instruction#1⟩ ⟨Instructions#2⟩} ⟨Instructions#3⟩ ⟧
    → ⟦ ⟨Instruction#1⟩ {⟨Instructions#2⟩} ⟨Instructions#3⟩ ⟧ ;

  // Helper data structure for list of registers.
  sort Rs | NoRs | MoRs(Reg, Rs) | scheme AppendRs(Rs, Rs) ;
  AppendRs(NoRs, #Rs) → #Rs ;
  AppendRs(MoRs(#Rn, #Rs1), #Rs2) → MoRs(#Rn, AppendRs(#Rs1, #Rs2)) ;

  // Helper conversion from Regs syntax to register list.
  | scheme XRegs(Regs) ;
  XRegs(⟦⟨Reg#r⟩⟧) → MoRs(#r, NoRs) ;
  XRegs(⟦⟨Reg#r1⟩-⟨Reg#r2⟩⟧) → XRegs1(#r1, #r2) ;
  XRegs(⟦⟨Reg#r⟩, ⟨Regs#Rs⟩⟧) → MoRs(#r, XRegs(#Rs)) ;
  XRegs(⟦⟨Reg#r1⟩-⟨Reg#r2⟩, ⟨Regs#Rs⟩⟧) → AppendRs(XRegs1(#r1, #r2), XRegs(#Rs)) ;

  | scheme XRegs1(Reg, Reg) ;
  XRegs1(#r, #r) → MoRs(#r, NoRs) ;
  default XRegs1(#r1, #r2) → XRegs2(#r1, #r2) ;

  | scheme XRegs2(Reg, Reg) ;
  XRegs2(⟦R0⟧, #r2) → MoRs(⟦R0⟧, XRegs1(⟦R1⟧, #r2)) ;
  XRegs2(⟦R1⟧, #r2) → MoRs(⟦R1⟧, XRegs1(⟦R2⟧, #r2)) ;
  XRegs2(⟦R2⟧, #r2) → MoRs(⟦R2⟧, XRegs1(⟦R3⟧, #r2)) ;
  XRegs2(⟦R3⟧, #r2) → MoRs(⟦R3⟧, XRegs1(⟦R4⟧, #r2)) ;
  XRegs2(⟦R4⟧, #r2) → MoRs(⟦R4⟧, XRegs1(⟦R5⟧, #r2)) ;
  XRegs2(⟦R5⟧, #r2) → MoRs(⟦R5⟧, XRegs1(⟦R6⟧, #r2)) ;
  XRegs2(⟦R6⟧, #r2) → MoRs(⟦R6⟧, XRegs1(⟦R7⟧, #r2)) ;
  XRegs2(⟦R7⟧, #r2) → MoRs(⟦R7⟧, XRegs1(⟦R8⟧, #r2)) ;
  XRegs2(⟦R8⟧, #r2) → MoRs(⟦R8⟧, XRegs1(⟦R9⟧, #r2)) ;
  XRegs2(⟦R9⟧, #r2) → MoRs(⟦R9⟧, XRegs1(⟦R10⟧, #r2)) ;
  XRegs2(⟦R10⟧, #r2) → MoRs(⟦R10⟧, XRegs1(⟦R11⟧, #r2)) ;
  XRegs2(⟦R11⟧, #r2) → MoRs(⟦R11⟧, XRegs1(⟦R12⟧, #r2)) ;
  XRegs2(⟦R12⟧, #r2) → MoRs(⟦R12⟧, NoRs) ;
  XRegs1(⟦SP⟧, #r2) → error⟦MinARM32 error: Cannot use SP in Regs range.⟧ ;
  XRegs1(⟦LR⟧, #r2) → error⟦MinARM32 error: Cannot use LR in Regs range.⟧ ;
  XRegs1(⟦PC⟧, #r2) → error⟦MinARM32 error: Cannot use PC in Regs range.⟧ ;
  
  // Helpers to insert computed assembly constants.
  sort Constant | scheme Immediate(Computed) | scheme Reference(Computed) ;
  Immediate(#x) → ⟦#⟨INT#x⟩⟧ ;
  Reference(#id) → ⟦&⟨Label#id⟩⟧ ;

  sort Mem | scheme FrameAccess(Computed) ;
  FrameAccess(#x)
    → FrameAccess1(#x, ⟦ [R12, ⟨Constant Immediate(#x)⟩] ⟧, ⟦ [R12, -⟨Constant Immediate(⟦0-#x⟧)⟩] ⟧) ;
  | scheme FrameAccess1(Computed, Mem, Mem) ;
  FrameAccess1(#x, #pos, #neg) → FrameAccess2(⟦ #x ≥ 0 ? #pos : #neg ⟧) ;
  | scheme FrameAccess2(Computed) ;
  FrameAccess2(#mem) → #mem ;

  sort Instruction | scheme AddConstant(Reg, Reg, Computed) ;
  AddConstant(#Rd, #Rn, #x)
    → AddConstant1(#x,
		   ⟦ ADD ⟨Reg#Rd⟩, ⟨Reg#Rn⟩, ⟨Constant Immediate(#x)⟩ ⟧,
		   ⟦ SUB ⟨Reg#Rd⟩, ⟨Reg#Rn⟩, ⟨Constant Immediate(⟦0-#x⟧)⟩ ⟧) ;
  | scheme AddConstant1(Computed, Instruction, Instruction) ;
  AddConstant1(#x, #pos, #neg) → AddConstant2(⟦ #x ≥ 0 ? #pos : #neg ⟧) ;
  | scheme AddConstant2(Computed) ;
  AddConstant2(#add) → #add ;

////////////////////////////////////////////////////////////////////////
// 3. COMPILER FROM MiniC TO MinARM32
////////////////////////////////////////////////////////////////////////

  // HACS doesn't like to compile with Computed sort
  // unless there exists a scheme that can generate Computed
  sort Computed | scheme Dummy ;
  Dummy → ⟦ 0 ⟧;

  // MAIN SCHEME

  sort Instructions  |  scheme Compile(Prog) ;
  Compile(#1) → P2(P1(#1), #1) ;

  // PASS 1

  // Result sort for first pass, with join operation.
  sort After1 | Data1(Instructions, FT) | scheme Join1(After1, After1) ;
  Join1(Data1(#1, #ft1), Data1(#2, #ft2))
    → Data1(⟦ { ⟨Instructions#1⟩ } ⟨Instructions#2⟩ ⟧, AppendFT(#ft1, #ft2)) ;

  // Function to return type environment (list of pairs with append).
  sort FT | NoFT | MoFT(ID, Type, FT) | scheme AppendFT(FT, FT) ;
  AppendFT(NoFT, #ft2) → #ft2 ;
  AppendFT(MoFT(#id1, #T1, #ft1), #ft2) → MoFT(#id1, #T1, AppendFT(#ft1, #ft2)) ;

  // Pass 1 recursion.
  sort After1 | scheme P1(Program) ;
  P1(⟦⟨Declarations#Ds⟩⟧) → P1Ds(#Ds) ;

  sort After1 | scheme P1Ds(Declarations);  // Def. \ref{def:P}.
  P1Ds(⟦⟨Declaration#D⟩ ⟨Declarations#Ds⟩⟧) → Join1(D(#D), P1Ds(#Ds)) ;
  P1Ds(⟦⟧) → Data1(⟦⟧, NoFT) ;

  // \sem{D} scheme (Def. \ref{def:D}).
  
  sort After1 | scheme D(Declaration) ;
  D(⟦ function ⟨Type#T⟩ f ⟨ArgumentSignature#As⟩ { ⟨Statements#S⟩ } ⟧)
    → Data1(⟦⟧, MoFT(⟦f⟧, #T, NoFT)) ;

  // PASS 2
  
  sort Label | scheme Ident2Label(Identifier) ;
  Ident2Label(⟦⟨Identifier#i⟩⟧) → #i;
  

  // Pass 2 strategy: first load type environment $ρ$ then tail-call recursion.
  sort Instructions | scheme P2(After1, Program) ;
  P2(Data1(#1, #ft1), #P) → P2Load(#1, #ft1, #P) ;

  // Type environment ($ρ$) is split in two components (by used sorts).
  attribute ↓ft{ID : Type} ;	// map from function name to return type
  attribute ↓vt{ID : Local} ;	// map from local variable name to type\&location
  sort Local | RegLocal(Type, Reg) | FrameLocal(Type, Computed) ;  // type\&location

  // Other inherited attributes.
  attribute ↓return(Label) ;		// label of return code
  attribute ↓true(Label) ;		// label to jump for true result
  attribute ↓false(Label) ;		// label to jump for false result
  attribute ↓value(Reg) ;		// register for expression result
  attribute ↓offset(Computed) ;		// frame offset for first unused local
  attribute ↓unused(Rs) ;		// list of unused registers
  
  // Pass 2 Loader: extract type environment $ρ$ and emit pass 1 directives.
  sort Instructions | scheme P2Load(Instructions, FT, Prog) ↓ft ↓vt ;
  P2Load(#is, MoFT(⟦f⟧, #T, #ft), #P) → P2Load(#is, #ft, #P) ↓ft{⟦f⟧ : #T} ;
  P2Load(#is, NoFT, #P) → ⟦ { ⟨Instructions#is⟩ } ⟨Instructions P(#P)⟩ ⟧ ;

  // Pass 2 recursion.
  sort Instructions | scheme P(Prog) ↓ft ↓vt ;
  P(⟦ ⟨FuncDecls#Ds⟩ ⟧) → Ds(#Ds) ;

  sort Instructions | scheme Ds(FuncDecls) ↓ft ↓vt ;
  Ds(⟦ ⟨FuncDecl#D⟩ ⟨FuncDecls#Ds⟩ ⟧) → ⟦ { ⟨Instructions F(#D)⟩ } ⟨Instructions Ds(#Ds)⟩ ⟧ ;
  Ds(⟦⟧) → ⟦⟧ ;

  // \sem{F} scheme (Def. \ref{def:F}), with argument signature iteration helpers.
  
  sort Instructions | scheme F(FuncDecl) ↓ft ↓vt ;
  F(⟦ function ⟨Type#T⟩ f ⟨ArgumentSignature#AS⟩ { ⟨Statements#S⟩ } ⟧) → ⟦
	f	STMFD SP!, {R4-R11,LR}
		MOV R12, SP
		{ ⟨Instructions AS(#AS, XRegs(⟦R0-R3⟧), #S) ↓return(⟦L⟧)⟩ }
	L	MOV SP, R12
		LDMFD SP!, {R4-R11,PC}
  ⟧ ;
  
  sort Instructions | scheme AS(ArgumentSignature, Rs, Statements) ↓ft ↓vt ↓return ;
  AS(⟦ () ⟧, #Rs, #S) → S(#S) ↓offset(⟦0-4⟧) ↓unused(XRegs(⟦R4-R5⟧)) ;
  AS(⟦ ( ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ) ⟧, MoRs(#r, #Rs), #S)
    → AS2(#TIT, #Rs, #S) ↓vt{⟦a⟧ : RegLocal(#T, #r)} ;

  sort Instructions | scheme AS2(TypeIdentifierTail, Rs, Statements) ↓ft ↓vt ↓return ;
  AS2(⟦ ⟧, #Rs, #S) → S(#S) ↓offset(⟦0-4⟧) ↓unused(XRegs(⟦R4-R5⟧)) ;
  AS2(⟦ , ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ⟧, MoRs(#r, #Rs), #S)
    → AS2(#TIT, #Rs, #S) ↓vt{⟦a⟧ : RegLocal(#T, #r)} ;
  AS2(⟦ , ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ⟧, NoRs, #S)
    → error⟦More than four arguments to function not allowed.⟧ ;


}
