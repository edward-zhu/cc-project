module org.rikarika.cc.Pr1
{

/* space matching */
space [ \n\t]
    | "/*" ([^*]|[*][*]*[^*/])*[*]+[/] // match multiline comment w/o nesting
    | "//" .*; // match single line comment

/* lexical analysis */
token INT   | ⟨DIGIT⟩+ ;
token ID    | [a-zA-Z$_][a-zA-Z0-9$_]* ;
token STRING | \"([^\"\\]|[\\]([nt\"\\]|(x⟨HEX⟩⟨HEX⟩)|(⟨OCT⟩⟨OCT⟩?⟨OCT⟩?)|\n))*\";
token MAINFUNCID | "function int main"; // main function matching (described in doc.)

token fragment DIGIT | [0-9] ;
token fragment HEX | [0-9a-fA-F] ;
token fragment OCT | [0-7] ;

/* syntatic analysis */

/* expressions combination */
sort MoreExp
    | ⟦,⟨Exp⟩⟨MoreExp⟩⟧
    | ⟦⟧
    ;

/* expression (in precedence order) */
sort Exp
    /* l-value */
    | ⟦⟨ID⟩⟧@10      // identifer
    | ⟦*⟨Exp@9⟩⟧@10    // derefer

    | sugar ⟦(⟨Exp#⟩)⟧@9 → Exp#
    | ⟦⟨STRING⟩⟧@8  // string
    | ⟦⟨INT⟩⟧@8     // Integer

    /* unary opers */
    | ⟦null(⟨Type⟩)⟧@8      // null
    | ⟦sizeof(⟨Type⟩)⟧@8    // sizeof
    | ⟦!⟨Exp⟩⟧@7    // logical negative
    | ⟦-⟨Exp⟩⟧@7    // negative
    | ⟦+⟨Exp⟩⟧@7    // plus
    | ⟦&⟨Exp⟩⟧@7    // reference

    | ⟦⟨Exp@9⟩()⟧@8
    | ⟦⟨Exp@9⟩( ⟨Exp@0⟩⟨MoreExp⟩ )⟧@8

    /* binary opers - arithmetic */
    | ⟦⟨Exp@6⟩*⟨Exp@7⟩⟧@6 // multiply, divide\
    | ⟦⟨Exp@6⟩/⟨Exp@7⟩⟧@6
    | ⟦⟨Exp@6⟩%⟨Exp@7⟩⟧@6 // modulo
    | ⟦⟨Exp@5⟩+⟨Exp@6⟩⟧@5 // plus, minus
    | ⟦⟨Exp@5⟩-⟨Exp@6⟩⟧@5 

    /* binary opers - comparison */
    | ⟦⟨Exp@4⟩<⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩<=⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩>⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩>=⟨Exp@5⟩⟧@4

    /* binary opers - logical */
    | ⟦⟨Exp@3⟩!=⟨Exp@4⟩⟧@3 // neq
    | ⟦⟨Exp@3⟩==⟨Exp@4⟩⟧@3 // eq
    | ⟦⟨Exp@2⟩&&⟨Exp@3⟩⟧@2 // and
    | ⟦⟨Exp@1⟩||⟨Exp@2⟩⟧@1 // or
    ;

sort MoreType
    | ⟦ , ⟨Type⟩⟨MoreType⟩ ⟧
    | ⟦⟧
    ;

sort Type
    | ⟦ int ⟧@2
    | ⟦ char ⟧@2
    | ⟦ *⟨Type@2⟩ ⟧@2
    | sugar ⟦ (⟨Type#⟩) ⟧@2 → Type#
    | ⟦ ⟨Type@2⟩ ( )⟧@1
    | ⟦ ⟨Type@2⟩ (⟨Type⟩⟨MoreType⟩)⟧@1
    ;


sort MoreStmt
    | ⟦⟨Stmt⟩⟨MoreStmt⟩⟧
    | ⟦⟧
    ;

sort Stmt
    | ⟦var⟨Type⟩⟨ID⟩ ;⟧
    | ⟦⟨Exp@10⟩ = ⟨Exp⟩ ;⟧
    | ⟦if (⟨Exp⟩) ⟨Stmt⟩⟧
    | ⟦if (⟨Exp⟩) ⟨Stmt⟩ else ⟨Stmt⟩⟧
    | ⟦while (⟨Exp⟩) ⟨Stmt⟩ ⟧
    | ⟦return ⟨Exp⟩ ;⟧
    | ⟦{⟨MoreStmt⟩}⟧
    ;

sort TypeIds
    | ⟦,⟨Type⟩ ⟨ID⟩ ⟨TypeIds⟩⟧
    | ⟦⟧
    ;

sort ParamDecl
    | ⟦(⟨Type⟩ ⟨ID⟩ ⟨TypeIds⟩)⟧
    | ⟦()⟧
    ;

sort FuncDecl
    | ⟦function ⟨Type⟩ ⟨ID⟩ ⟨ParamDecl⟩ { ⟨MoreStmt⟩ }⟧
    ;

sort MainFuncDecl
    | ⟦⟨MAINFUNCID⟩ ⟨ParamDecl⟩ { ⟨MoreStmt⟩ }⟧
    ;

sort FuncDecls
    | ⟦⟨FuncDecl⟩⟨FuncDecls⟩⟧
    | ⟦⟨FuncDecl⟩⟧
    ;

main sort Prog
    | ⟦⟨FuncDecls⟩⟨MainFuncDecl⟩⟧
    | ⟦⟨MainFuncDecl⟩⟨FuncDecls⟩⟧
    | ⟦⟨MainFuncDecl⟩⟧
    | ⟦⟨FuncDecls⟩⟨MainFuncDecl⟩⟨FuncDecls⟩⟧
    ;

}
