module org.rikarika.cc.Pr2
{

/* space matching */
space [ \n\t]
    | "/*" ([^*]|[*][*]*[^*/])*[*]+[/] // match multiline comment w/o nesting
    | "//" .*; // match single line comment

/* lexical analysis */
token INT   | ⟨DIGIT⟩+ ;
token ID    | [a-zA-Z$_][a-zA-Z0-9$_]* ;
token STRING | \"([^\"\\]|[\\]([nt\"\\]|(x⟨HEX⟩⟨HEX⟩)|(⟨OCT⟩⟨OCT⟩?⟨OCT⟩?)|\n))*\";
// token MAINFUNCID | "function int main"; // main function matching (described in doc.)

token fragment DIGIT | [0-9] ;
token fragment HEX | [0-9a-fA-F] ;
token fragment OCT | [0-7] ;

/* syntatic analysis */

/* expressions combination */
sort MoreExp
    | ⟦,⟨Exp⟩⟨MoreExp⟩⟧
    | ⟦⟧
    ;

/* expression (in precedence order) */
sort Exp
    /* l-value */
    | ⟦⟨ID⟩⟧@10      // identifer
    | ⟦*⟨Exp@9⟩⟧@10    // derefer

    | sugar ⟦(⟨Exp#⟩)⟧@9 → Exp#
    | ⟦⟨STRING⟩⟧@8  // string
    | ⟦⟨INT⟩⟧@8     // Integer

    /* unary opers */
    | ⟦null(⟨Type⟩)⟧@8      // null
    | ⟦sizeof(⟨Type⟩)⟧@8    // sizeof
    | ⟦!⟨Exp⟩⟧@7    // logical negative
    | ⟦-⟨Exp⟩⟧@7    // negative
    | ⟦+⟨Exp⟩⟧@7    // plus
    | ⟦&⟨Exp⟩⟧@7    // reference

    | ⟦⟨Exp@9⟩()⟧@8
    | ⟦⟨Exp@9⟩( ⟨Exp@0⟩⟨MoreExp⟩ )⟧@8

    /* binary opers - arithmetic */
    | ⟦⟨Exp@6⟩*⟨Exp@7⟩⟧@6 // multiply, divide\
    | ⟦⟨Exp@6⟩/⟨Exp@7⟩⟧@6
    | ⟦⟨Exp@6⟩%⟨Exp@7⟩⟧@6 // modulo
    | ⟦⟨Exp@5⟩+⟨Exp@6⟩⟧@5 // plus, minus
    | ⟦⟨Exp@5⟩-⟨Exp@6⟩⟧@5 

    /* binary opers - comparison */
    | ⟦⟨Exp@4⟩<⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩<=⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩>⟨Exp@5⟩⟧@4
    | ⟦⟨Exp@4⟩>=⟨Exp@5⟩⟧@4

    /* binary opers - logical */
    | ⟦⟨Exp@3⟩!=⟨Exp@4⟩⟧@3 // neq
    | ⟦⟨Exp@3⟩==⟨Exp@4⟩⟧@3 // eq
    | ⟦⟨Exp@2⟩&&⟨Exp@3⟩⟧@2 // and
    | ⟦⟨Exp@1⟩||⟨Exp@2⟩⟧@1 // or
    ;

sort MoreType
    | ⟦ , ⟨Type⟩⟨MoreType⟩ ⟧
    | ⟦⟧
    ;

sort Type
    | ⟦ int ⟧@2
    | ⟦ char ⟧@2
    | ⟦ *⟨Type@2⟩ ⟧@2
    | sugar ⟦ (⟨Type#⟩) ⟧@2 → Type#
    | ⟦ ⟨Type@2⟩ ( )⟧@1
    | ⟦ ⟨Type@2⟩ (⟨Type⟩⟨MoreType⟩)⟧@1
    ;


sort MoreStmt
    | ⟦⟨Stmt⟩⟨MoreStmt⟩⟧
    | ⟦⟧
    ;

sort Stmt
    | ⟦var⟨Type⟩⟨ID⟩ ;⟧
    | ⟦⟨Exp@10⟩ = ⟨Exp⟩ ;⟧
    | ⟦if (⟨Exp⟩) ⟨Stmt⟩⟧
    | ⟦if (⟨Exp⟩) ⟨Stmt⟩ else ⟨Stmt⟩⟧
    | ⟦while (⟨Exp⟩) ⟨Stmt⟩ ⟧
    | ⟦return ⟨Exp⟩ ;⟧
    | ⟦{⟨MoreStmt⟩}⟧
    ;

sort TypeIds
    | ⟦,⟨Type⟩ ⟨ID⟩ ⟨TypeIds⟩⟧
    | ⟦⟧
    ;

sort ParamDecl
    | ⟦(⟨Type⟩ ⟨ID⟩ ⟨TypeIds⟩)⟧
    | ⟦()⟧
    ;

sort FuncDecl
    | ⟦function ⟨Type⟩ ⟨ID⟩ ⟨ParamDecl⟩ { ⟨MoreStmt⟩ }⟧
    ;

sort FuncDecls
    | ⟦⟨FuncDecl⟩⟨FuncDecls⟩⟧
    | ⟦⟨FuncDecl⟩⟧
    ;

main sort Prog
    | ⟦⟨FuncDecls⟩⟧
    ;

/***
    Semantic Analysis
***/

/* Type Check Functions */

sort Type | scheme TCToInt(Type);
TCToInt(⟦int⟧) → ⟦int⟧;
TCToInt(⟦* ⟨Type#⟩⟧) → ⟦int⟧;
default TCToInt(#) → error ⟦must be a pointer or int value⟧;

sort Type | scheme TCPToInt(Type, Type);
TCPToInt(⟦int⟧, ⟦int⟧) → ⟦int⟧;

sort Type | scheme TCDeRef(Type);
TCDeRef(⟦* ⟨Type#⟩⟧) → #;

sort Type | scheme TCPlusMinus(Type, Type);
TCPlusMinus(⟦*⟨Type#⟩⟧, ⟦int⟧) → ⟦*⟨Type#⟩⟧;
TCPlusMinus(⟦int⟧, ⟦int⟧) → ⟦int⟧;
default TCPlusMinus(#1, #2) → error ⟦type mismatch⟧;

sort Type | scheme TCEq(Type, Type);
TCEq(⟦int⟧, ⟦int⟧) → ⟦int⟧;
TCEq(⟦* ⟨Type#⟩⟧, ⟦* ⟨Type#⟩⟧) → ⟦ int ⟧;
default TCEq(#1, #2) → error ⟦type mismatch⟧;

sort Type | scheme TCComp(Type, Type);
TCComp(⟦int⟧, ⟦int⟧) → ⟦int⟧;
default TCComp(#1, #2) → error ⟦comparison oper can only take two int oprands.⟧;

sort Type | scheme TCLogical(Type, Type);
TCLogical(#1, #2) → TCPToInt(TCToInt(#1), TCToInt(#2));

sort Type | scheme Equal(Type, Type);
Equal(#, #) → #;
Equal(⟦*⟨Type#1⟩⟧, ⟦*⟨Type#2⟩⟧) → ⟦* ⟨Type Equal(#1, #2)⟩⟧;
default Equal(#1, #2) → error ⟦inequal type⟧;

attribute ↑t(Type);

sort Type | scheme GetType(Exp);
GetType(⟦⟨Exp#⟩⟧) → GetType2(⟦⟨Exp Ee(#)⟩⟧);
{
    | scheme GetType2(Exp);
    GetType2(⟦⟨Exp# ↑t(#t)⟩⟧) → #t;
}

sort Type | scheme Equal3(Type, Type, Type);
Equal3(#1, #2, #2) → #1;
default Equal3(#1, #2, #3) → error ⟦type mismatch 3⟧;

sort Type | scheme Equal3a(Type, MoreType, MoreExp);
Equal3a(#1, ⟦,⟨Type#t1⟩⟨MoreType#mt⟩⟧, ⟦,⟨Exp#e ↑t(#t1)⟩⟨MoreExp#me⟩⟧) → Equal3a(#1, #mt, #me);
Equal3a(#1, ⟦⟧, ⟦⟧) → #1;
default Equal3a(#1, #2, #3) → error ⟦type mismatch 3a⟧;

sort Type | scheme Equal5(Type, Type, Type, MoreType, MoreExp);
Equal5(#1, #2, #2, #mt, #me) → Equal3a(#1, #mt, #me);
default Equal5(#1, #2, #3, #4, #5) → error ⟦type mismatch 5⟧;

/* Type Check - Expression */

sort Exp | ↑t;
⟦⟨INT#⟩⟧↑t(⟦int⟧);
⟦⟨STRING#⟩⟧↑t(⟦* char⟧);
⟦null(⟨Type#⟩)⟧↑t(#);
⟦sizeof(⟨Type#⟩)⟧↑t(⟦int⟧);
⟦* ⟨Exp# ↑t(#t)⟩⟧↑t(TCDeRef(#t));
⟦& ⟨Exp# ↑t(#t)⟩⟧↑t(⟦* ⟨Type#t⟩⟧);
⟦⟨Exp#1 ↑t(⟦⟨Type#t⟩( )⟧)⟩()⟧↑t(#t);
⟦⟨Exp#1 ↑t(⟦⟨Type#t⟩(⟨Type#t1⟩⟨MoreType#mt⟩)⟧)⟩ (⟨Exp#2 ↑t(#t2)⟩⟨MoreExp#me⟩)⟧↑t(Equal5(#t, #t1, #t2, #mt, #me));

// arithmatic op
⟦(⟨Exp#1 ↑t(#t1)⟩ + ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(TCPlusMinus(#t1, #t2));
⟦(⟨Exp#1 ↑t(#t1)⟩ * ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(Equal(#t1, #t2));

// equality op
⟦(⟨Exp#1 ↑t(#t1)⟩ == ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(TCEq(#t1, #t2));

// comparison op
⟦(⟨Exp#1 ↑t(#t1)⟩ < ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(TCComp(#t1, #t2));

// logical op
⟦(⟨Exp#1 ↑t(#t1)⟩ && ⟨Exp#2 ↑t(#t2)⟩)⟧↑t(TCLogical(#t1, #t2));


attribute ↓e{ID : Type};

sort MoreExp | scheme MEe(MoreExp) ↓e;
MEe(⟦, ⟨Exp#1⟩⟨MoreExp#me⟩⟧)↓e{:#MEe} → ⟦, ⟨Exp Ee(#1) ↓e{:#MEe}⟩⟨MoreExp MEe(#me) ↓e{:#MEe}⟩⟧;
MEe(⟦⟧)↓e{:#MEe} → ⟦⟧;

sort Exp | scheme Ee(Exp) ↓e;
Ee(⟦⟨ID#v⟩⟧)↓e{#v: #t} → ⟦⟨ID#v⟩⟧ ↑t(#t);
Ee(⟦⟨ID#v⟩⟧)↓e{¬#v} → error ⟦undefined id⟧;
Ee(⟦⟨STRING#1⟩⟧) → ⟦⟨STRING#1⟩⟧↑t(⟦* char⟧);
Ee(⟦⟨INT#1⟩⟧) → ⟦⟨INT#1⟩⟧↑t(⟦int⟧);
Ee(⟦sizeof(⟨Type#⟩)⟧) → ⟦sizeof(⟨Type#⟩)⟧ ↑t(⟦int⟧);
Ee(⟦null(⟨Type#⟩)⟧) → ⟦null(⟨Type#⟩)⟧ ↑t(#);
Ee(⟦*⟨Exp#1⟩⟧)↓e{:#Ee} → ⟦*⟨Exp Ee(#1)↓e{:#Ee}⟩⟧;
Ee(⟦&⟨Exp#1⟩⟧)↓e{:#Ee} → ⟦&⟨Exp Ee(#1)↓e{:#Ee}⟩⟧;
Ee(⟦⟨Exp#1⟩()⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩()⟧;
Ee(⟦⟨Exp#1⟩(⟨Exp#2⟩⟨MoreExp#3⟩)⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩(⟨Exp Ee(#2)↓e{:#Ee}⟩⟨MoreExp#3⟩)⟧;

// arithmatic op
Ee(⟦⟨Exp#1⟩ + ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ + ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;
Ee(⟦⟨Exp#1⟩ * ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ * ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;

// equality op
Ee(⟦⟨Exp#1⟩ == ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ == ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;

// comparison op
Ee(⟦⟨Exp#1⟩ < ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ < ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;

// logical op
Ee(⟦⟨Exp#1⟩ && ⟨Exp#2⟩⟧)↓e{:#Ee} → ⟦⟨Exp Ee(#1)↓e{:#Ee}⟩ && ⟨Exp Ee(#2)↓e{:#Ee}⟩⟧;

default Ee(#) → error ⟦invalid expression⟧;

/* Type Check - Statements */

sort MoreStmt | scheme Se2(MoreStmt) ↓e;
Se2(⟦var ⟨Type#t⟩⟨ID#v⟩; ⟨MoreStmt#1⟩⟧)↓e{:#Se2} →
    ⟦var ⟨Type#t⟩⟨ID#v⟩; ⟨MoreStmt Se2(#1)↓e{:#Se2} ↓e{#v:#t}⟩⟧;
Se2(⟦⟨Stmt#1⟩⟨MoreStmt#2⟩⟧)↓e{:#Se2}
    → ⟦⟨Stmt Se(#1)↓e{:#Se2}⟩ ⟨MoreStmt Se2(#2) ↓e{:#Se2}⟩⟧;
Se2(⟦⟧) → ⟦⟧;


sort Stmt | scheme Se(Stmt) ↓e;
Se(⟦⟨Exp#1⟩=⟨Exp#2⟩;⟧)↓e{:#Se}
    → SeB(⟦⟨Exp Ee(#1)↓e{:#Se}⟩= ⟨Exp Ee(#2)↓e{:#Se}⟩;⟧);
{
    | scheme SeB(Stmt);
    SeB(⟦⟨Exp#1 ↑t(#t1)⟩=⟨Exp#2 ↑t(#t2)⟩;⟧) →
        ⟦⟨Exp#1⟩ = ⟨Exp#2⟩;⟧↑t(Equal(#t1, #t2));
}
Se(⟦if(⟨Exp#1⟩) ⟨Stmt#s⟩⟧)↓e{:#Se} → SeIf(⟦if (⟨Exp Ee(#1) ↓e{:#Se}⟩) ⟨Stmt Se(#s) ↓e{:#Se}⟩⟧);
{
    | scheme SeIf(Stmt);
    SeIf(⟦if (⟨Exp#1 ↑t(#t)⟩) ⟨Stmt#s⟩⟧) → ⟦if (⟨Exp#1⟩) ⟨Stmt#s⟩⟧ ↑t(TCToInt(#t));
}
Se(⟦if(⟨Exp#1⟩) ⟨Stmt#s1⟩ else ⟨Stmt#s2⟩⟧) ↓e{:#Se} → SeIfElse(⟦if(⟨Exp Ee(#1)↓e{:#Se}⟩)
    ⟨Stmt Se(#s1) ↓e{:#Se}⟩ else ⟨Stmt Se(#s2) ↓e{:#Se}⟩⟧);
{
    | scheme SeIfElse(Stmt);
    SeIfElse(⟦if (⟨Exp#1 ↑t(#t)⟩) ⟨Stmt#s1⟩ else ⟨Stmt#s2⟩⟧) → ⟦if (⟨Exp#1⟩) ⟨Stmt#s1⟩ else ⟨Stmt#s2⟩⟧ ↑t(TCToInt(#t));
}
Se(⟦while (⟨Exp#1⟩) ⟨Stmt#s⟩⟧)↓e{:#Se} → SeWhile(⟦while (⟨Exp Ee(#1)↓e{:#Se}⟩) ⟨Stmt Se(#s) ↓e{:#Se}⟩⟧);
{
    | scheme SeWhile(Stmt);
    SeWhile(⟦while (⟨Exp#1 ↑t(#t)⟩) ⟨Stmt#s⟩⟧) → ⟦while (⟨Exp#1⟩) ⟨Stmt#s⟩⟧↑t(TCToInt(#t));
}
Se(⟦return ⟨Exp#1⟩ ;⟧)↓e{:#Se} → SeReturn(⟦return ⟨Exp Ee(#1) ↓e{:#Se}⟩;⟧) ↓e{:#Se};
{
    | scheme SeReturn(Stmt) ↓e;
    SeReturn(⟦return ⟨Exp#1 ↑t(#t)⟩ ;⟧)↓e{⟦ret⟧:#rt} → ⟦return ⟨Exp#1⟩ ;⟧↑t(Equal(#t, #rt));
    SeReturn(#)↓e{¬⟦ret⟧} → error ⟦internal error⟧;
}
Se(⟦{⟨MoreStmt#⟩}⟧)↓e{:#Se} → ⟦{⟨MoreStmt Se2(#) ↓e{:#Se}⟩}⟧;

attribute ↑d{ID : Type};

sort ParamDecl | ↑d;
⟦(⟨Type#t⟩ ⟨ID#v⟩ ⟨TypeIds#ts ↑d{:#d}⟩)⟧ ↑d{:#d} ↑d{#v:#t};
⟦()⟧↑d{};

sort TypeIds | ↑d;
⟦, ⟨Type#t⟩ ⟨ID#v⟩ ⟨TypeIds#ts ↑d{:#d}⟩⟧ ↑d{:#d} ↑d{#v:#t};
⟦⟧↑d{};


attribute ↓pt(Type);
attribute ↑mt(MoreType);

sort TypeIds | ↑mt;
⟦, ⟨Type#t⟩ ⟨ID#v⟩ ⟨TypeIds#ts ↑mt(#mt)⟩⟧ ↑mt(⟦, ⟨Type#t⟩⟨MoreType#mt⟩⟧);
⟦⟧↑mt(⟦⟧);

/* Type Check - Functions */

sort FuncDecl | scheme Fe(FuncDecl) ↓e;
Fe(⟦function ⟨Type#t⟩ ⟨ID#n⟩ ⟨ParamDecl#pd⟩ {⟨MoreStmt#s⟩}⟧) ↓e{#n:#nt} → error ⟦duplicate function name⟧;
Fe(⟦function ⟨Type#t⟩ ⟨ID#n⟩ () { ⟨MoreStmt#s⟩ }⟧) ↓e{:#Fe} →
    ⟦function ⟨Type#t⟩ ⟨ID#n⟩ () {
        ⟨MoreStmt Se2(#s) ↓e{:#Fe} ↓e{#n:⟦⟨Type#t⟩( )⟧} ↓e{⟦ret⟧:⟦⟨Type#t⟩⟧}⟩ }⟧;
Fe(⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩ ⟨TypeIds#ts ↑d{:#d} ↑mt(#mt)⟩) { ⟨MoreStmt#s⟩ }⟧) ↓e{:#Fe} →
    ⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩⟨TypeIds#ts⟩)
        { ⟨MoreStmt Se2(#s) ↓e{#n1:#t1} ↓e{:#Fe} ↓e{:#d}
            ↓e{#n:⟦⟨Type#t⟩(⟨Type#t1⟩⟨MoreType#mt⟩)⟧} ↓e{⟦ret⟧:⟦⟨Type#t⟩⟧}⟩}⟧;

sort FuncDecl | ↑d;
⟦function ⟨Type#t⟩ ⟨ID#n⟩ () { ⟨MoreStmt#s⟩ }⟧ ↑d{#n:⟦⟨Type#t⟩( )⟧};
⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩ ⟨TypeIds#ts ↑mt(#mt)⟩) { ⟨MoreStmt#s⟩ }⟧
    ↑d{#n:⟦⟨Type#t⟩ (⟨Type#t1⟩⟨MoreType#mt⟩)⟧};

sort FuncDecls | scheme Fe2(FuncDecls) ↓e;
Fe2(⟦⟨FuncDecl#fd⟩⟨FuncDecls#fds⟩⟧)↓e{:#Fe2} → Fe2B(⟦⟨FuncDecl Fe(#fd) ↓e{:#Fe2}⟩⟨FuncDecls#fds⟩⟧) ↓e{:#Fe2};
{
    | scheme Fe2B(FuncDecls) ↓e;
    Fe2B(⟦function ⟨Type#t⟩ ⟨ID#n⟩ () { ⟨MoreStmt#s⟩} ⟨FuncDecls#fds⟩⟧) ↓e{:#Fe2}
        → ⟦function ⟨Type#t⟩ ⟨ID#n⟩ () {⟨MoreStmt#s⟩} ⟨FuncDecls Fe2(#fds) ↓e{:#Fe2} ↓e{#n:⟦⟨Type#t⟩( )⟧}⟩⟧;
    Fe2B(⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩ ⟨TypeIds#ts ↑mt(#mt)⟩) { ⟨MoreStmt#s⟩ } ⟨FuncDecls#fds⟩⟧) ↓e{:#Fe2}
        → ⟦function ⟨Type#t⟩ ⟨ID#n⟩ (⟨Type#t1⟩⟨ID#n1⟩ ⟨TypeIds#ts⟩) {⟨MoreStmt#s⟩}
            ⟨FuncDecls Fe2(#fds) ↓e{:#Fe2} ↓e{#n:⟦⟨Type#t⟩ (⟨Type#t1⟩⟨MoreType#mt⟩)⟧}⟩⟧;
}
Fe2(⟦⟨FuncDecl#fd⟩⟧)↓e{:#Fe2} → ⟦⟨FuncDecl Fe(#fd) ↓e{:#Fe2}⟩⟧;

sort FuncDecls | ↑d;
⟦⟨FuncDecl#fd ↑d{:#d}⟩⟨FuncDecls#fds ↑d{:#ds}⟩⟧↑d{:#d} ↑d{:#ds};
⟦⟨FuncDecl#fd ↑d{:#d}⟩⟧↑d{:#d};

sort Prog | scheme TypeCheck(Prog);
TypeCheck(⟦⟨FuncDecls #fds⟩⟧) → MainCheck(⟦⟨FuncDecls Fe2(#fds)
    ↓e{⟦malloc⟧: ⟦* char(int)⟧}
    ↓e{⟦puts⟧: ⟦int(* char)⟧}
    ↓e{⟦puti⟧: ⟦int(int)⟧}
    ↓e{⟦atoi⟧: ⟦int(*char)⟧}
    ↓e{⟦div⟧: ⟦int(int, int)⟧}
    ↓e{⟦mod⟧: ⟦int(int, int)⟧}
    ⟩⟧);
{
    | scheme MainCheck(Prog);
    MainCheck(⟦⟨FuncDecls#fds ↑d{⟦main⟧:#t}⟩⟧) → ⟦⟨FuncDecls#fds⟩⟧↑t(Equal(#t, ⟦int(*char)⟧));
    MainCheck(⟦⟨FuncDecls#fds ↑d{¬⟦main⟧}⟩⟧) → error ⟦no main function.⟧;
}


}
