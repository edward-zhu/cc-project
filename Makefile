HACS= "$(HOME)/.hacs/bin/hacs"

main: Pr2.run

Pr1.run: Pr1.hx
	$(HACS) Pr1.hx

Pr2.run: Pr2.hx
	$(HACS) Pr2.hx

debug:
	$(HACS) Pr2.hx -e

