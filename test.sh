#!/bin/bash

echo "testing correct cases..."

for correct in samples/*.MC; do
    ./Pr1.run $correct | tee tmp
    if [[ $? == 0 ]]; then
        grep "Pr1" tmp > /dev/null
        if [[ $? == 1 ]]; then
            echo "-> Passed $correct."
        else
            echo "-> Failed $correct in Syntatic Phase"
            exit 1
        fi
    else
        echo "-> Failed $correct in Lexical Phase."
        exit 1
    fi
done

rm tmp

echo "tesing incorrect cases..."

for invalid in samples/*.badMC; do
    ./Pr1.run $invalid &> /dev/null
    if [[ $? != 0 ]]; then
        echo "-> Passed $invalid"
    else
        echo "-> Failed $invalid"
        exit 1
    fi
done

echo "passed all tests!"

exit 0
