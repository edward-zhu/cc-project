// [NYU Courant Institute] Compiler Construction/Fall 2016/Project Milestone 3 -*-hacs-*-
//
// Contents.
// 1. MiniC Lexical analysis and grammar
// 2. MinARM32 assembler grammar
// 3. Compiler from MiniC to MinARM32
//
// Refer to documentation in \url{http://cs.nyu.edu/courses/fall16/CSCI-GA.2130-001/}.

module edu.nyu.cs.cc.Pr3Base {

  
  ////////////////////////////////////////////////////////////////////////
  // 1. MiniC LEXICAL ANALYSIS AND GRAMMAR
  ////////////////////////////////////////////////////////////////////////

  // pr1: refers to http://cs.nyu.edu/courses/fall16/CSCI-GA.2130-001/project1/pr1.pdf

  // TOKENS (pr1:1.1).

  space [ \t\n\r] | '//' [^\n]* | '/*' ( [^*] | '*' [^/] )* '*/'  ; // Inner /* ignored

  token ID  	| ⟨LetterEtc⟩ (⟨LetterEtc⟩ | ⟨Digit⟩)* ;
  token INT	| ⟨Digit⟩+ ;
  token STR | "\"" ( [^\"\\\n] | \\ ⟨Escape⟩ )* "\"";

  token fragment Letter     | [A-Za-z] ;
  token fragment LetterEtc  | ⟨Letter⟩ | [$_] ;
  token fragment Digit      | [0-9] ;
  
  token fragment Escape  | [\n\\nt"] | "x" ⟨Hex⟩ ⟨Hex⟩ | ⟨Octal⟩;
  token fragment Hex     | [0-9A-Fa-f] ;
  token fragment Octal   | [0-7] | [0-7][0-7] | [0-7][0-7][0-7];
 
  // PROGRAM (pr1:2.6)

  main sort Program  |  ⟦ ⟨Declarations⟩ ⟧ ;

  // DECLARATIONS (pr1:1.5)

  sort Declarations | ⟦ ⟨Declaration⟩ ⟨Declarations⟩ ⟧ | ⟦⟧ ;

  sort Declaration
    |  ⟦ function ⟨Type⟩ ⟨Identifier⟩ ⟨ArgumentSignature⟩ { ⟨Statements⟩ } ⟧
    ;

  sort ArgumentSignature
    |  ⟦ ( ) ⟧
    |  ⟦ ( ⟨Type⟩ ⟨Identifier⟩ ⟨TypeIdentifierTail⟩ ) ⟧
    ;
  sort TypeIdentifierTail |  ⟦ , ⟨Type⟩ ⟨Identifier⟩ ⟨TypeIdentifierTail⟩ ⟧  |  ⟦ ⟧ ;

  // STATEMENTS (pr1:1.4)

  sort Statements | ⟦ ⟨Statement⟩ ⟨Statements⟩ ⟧ | ⟦⟧ ;

  sort Statement
    |  ⟦ { ⟨Statements⟩ } ⟧
    |  ⟦ var ⟨Type⟩ ⟨Identifier⟩ ; ⟧
    |  ⟦ ⟨Expression⟩ = ⟨Expression⟩ ; ⟧
    |  ⟦ if ( ⟨Expression⟩ ) ⟨IfTail⟩ ⟧
    |  ⟦ while ( ⟨Expression⟩ ) ⟨Statement⟩ ⟧
    |  ⟦ return ⟨Expression⟩ ; ⟧
    ;

  sort IfTail | ⟦ ⟨Statement⟩ else ⟨Statement⟩ ⟧ | ⟦ ⟨Statement⟩ ⟧ ;

  // TYPES (pr1:1.3)

  sort Type
    |  ⟦ int ⟧@3
    |  ⟦ char ⟧@3
    |  ⟦ ( ⟨Type⟩ )⟧@3
    |  ⟦ ⟨Type@2⟩ ( ⟨TypeList⟩ )⟧@2
    |  ⟦ * ⟨Type@1⟩ ⟧@1
    ;
    
  sort TypeList | ⟦ ⟨Type⟩ ⟨TypeListTail⟩ ⟧ | ⟦⟧;
  sort TypeListTail | ⟦ , ⟨Type⟩ ⟨TypeListTail⟩ ⟧ | ⟦⟧;  

  // EXPRESSIONS (pr1:2.2)

  sort Expression

    |  sugar ⟦ ( ⟨Expression#e⟩ ) ⟧@10 → #e

    |  ⟦ ⟨Integer⟩ ⟧@10
    |  ⟦ ⟨String⟩ ⟧@10
    |  ⟦ ⟨Identifier⟩ ⟧@10

    |  ⟦ ⟨Expression@9⟩ ( ⟨ExpressionList⟩ ) ⟧@9
    |  ⟦ null ( ⟨Type⟩ ) ⟧@9
    |  ⟦ sizeof ( ⟨Type⟩ )⟧@9

    |  ⟦ ! ⟨Expression@8⟩ ⟧@8
    |  ⟦ - ⟨Expression@8⟩ ⟧@8
    |  ⟦ + ⟨Expression@8⟩ ⟧@8
    |  ⟦ * ⟨Expression@8⟩ ⟧@8
    |  ⟦ & ⟨Expression@8⟩ ⟧@8

    |  ⟦ ⟨Expression@7⟩ * ⟨Expression@8⟩ ⟧@7

    |  ⟦ ⟨Expression@6⟩ + ⟨Expression@7⟩ ⟧@6
    |  ⟦ ⟨Expression@6⟩ - ⟨Expression@7⟩ ⟧@6

    |  ⟦ ⟨Expression@6⟩ < ⟨Expression@6⟩ ⟧@5
    |  ⟦ ⟨Expression@6⟩ > ⟨Expression@6⟩ ⟧@5
    |  ⟦ ⟨Expression@6⟩ <= ⟨Expression@6⟩ ⟧@5
    |  ⟦ ⟨Expression@6⟩ >= ⟨Expression@6⟩ ⟧@5

    |  ⟦ ⟨Expression@5⟩ == ⟨Expression@5⟩ ⟧@4
    |  ⟦ ⟨Expression@5⟩ != ⟨Expression@5⟩ ⟧@4

    |  ⟦ ⟨Expression@3⟩ && ⟨Expression@4⟩ ⟧@3

    |  ⟦ ⟨Expression@2⟩ || ⟨Expression@3⟩ ⟧@2
    ;
    
  // Helper to describe actual list of arguments of function call.
  sort ExpressionList | ⟦ ⟨Expression⟩ ⟨ExpressionListTail⟩ ⟧  |  ⟦⟧ ;
  sort ExpressionListTail | ⟦ , ⟨Expression⟩ ⟨ExpressionListTail⟩ ⟧  |  ⟦⟧ ;  

  sort Integer		| ⟦ ⟨INT⟩ ⟧ ;
  sort String		| ⟦ ⟨STR⟩ ⟧ ;
  sort Identifier	| symbol ⟦⟨ID⟩⟧ ;

 
  ////////////////////////////////////////////////////////////////////////
  // 2. MinARM32 ASSEMBLER GRAMMAR
  ////////////////////////////////////////////////////////////////////////

  // arm: refers to http://cs.nyu.edu/courses/fall14/CSCI-GA.2130-001/pr3/MinARM32.pdf
 
  // Instructions.
  sort Instructions | ⟦ ⟨Instruction⟩ ⟨Instructions⟩ ⟧ | ⟦⟧ ;

  // Directives (arm:2.1)
  sort Instruction
    | ⟦DEF ⟨ID⟩ = ⟨Integer⟩ ¶⟧  // define identifier
    | ⟦¶⟨Label⟩ ⟧               // define address label
    | ⟦DCI ⟨Integers⟩ ¶⟧        // allocate integers
    | ⟦DCS ⟨String⟩ ¶⟧          // allocate strings
    | ⟦⟨Op⟩ ¶⟧                  // machine instruction
    ;

  sort Integers | ⟦ ⟨Integer⟩, ⟨Integers⟩ ⟧ | ⟦ ⟨Integer⟩ ⟧ ;

  sort Label | symbol ⟦⟨ID⟩⟧ ;
 
  // Syntax of individual machine instructions (arm:2.2).
  sort Op

    | ⟦MOV ⟨Reg⟩, ⟨Arg⟩ ⟧		// move
    | ⟦MVN ⟨Reg⟩, ⟨Arg⟩ ⟧		// move not
    | ⟦ADD ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// add
    | ⟦SUB ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// subtract
    | ⟦RSB ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// reverse subtract
    | ⟦AND ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// bitwise and
    | ⟦ORR ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// bitwise or
    | ⟦EOR ⟨Reg⟩, ⟨Reg⟩, ⟨Arg⟩ ⟧	// bitwise exclusive or
    | ⟦CMP ⟨Reg⟩, ⟨Arg⟩ ⟧	    	// compare
    | ⟦MUL ⟨Reg⟩, ⟨Reg⟩, ⟨Reg⟩ ⟧	// multiply

    | ⟦B ⟨Label⟩ ⟧			// branch always
    | ⟦BEQ ⟨Label⟩ ⟧			// branch if equal
    | ⟦BNE ⟨Label⟩ ⟧			// branch if not equal
    | ⟦BGT ⟨Label⟩ ⟧			// branch if greater than
    | ⟦BLT ⟨Label⟩ ⟧			// branch if less than
    | ⟦BGE ⟨Label⟩ ⟧			// branch if greater than or equal
    | ⟦BLE ⟨Label⟩ ⟧			// branch if less than or equal
    | ⟦BL ⟨Label⟩ ⟧			// branch and link

    | ⟦LDR ⟨Reg⟩, ⟨Mem⟩ ⟧		// load register from memory
    | ⟦STR ⟨Reg⟩, ⟨Mem⟩ ⟧		// store register to memory

    | ⟦LDMFD ⟨Reg⟩! , {⟨Regs⟩} ⟧ 	// load multiple fully descending (pop)
    | ⟦STMFD ⟨Reg⟩! , {⟨Regs⟩} ⟧	// store multiple fully descending (push)
    ;

  // Arguments.

  sort Reg	| ⟦R0⟧ | ⟦R1⟧ | ⟦R2⟧ | ⟦R3⟧ | ⟦R4⟧ | ⟦R5⟧ | ⟦R6⟧ | ⟦R7⟧
		| ⟦R8⟧ | ⟦R9⟧ | ⟦R10⟧ | ⟦R11⟧ | ⟦R12⟧ | ⟦SP⟧ | ⟦LR⟧ | ⟦PC⟧ ;

  sort Arg | ⟦⟨Constant⟩⟧ | ⟦⟨Reg⟩⟧ | ⟦⟨Reg⟩, LSL ⟨Constant⟩⟧ | ⟦⟨Reg⟩, LSR ⟨Constant⟩⟧ ;

  sort Mem | ⟦[⟨Reg⟩, ⟨Sign⟩⟨Arg⟩]⟧ ;
  sort Sign | ⟦+⟧ | ⟦-⟧ | ⟦⟧ ;

  sort Regs | ⟦⟨Reg⟩⟧ | ⟦⟨Reg⟩-⟨Reg⟩⟧ | ⟦⟨Reg⟩, ⟨Regs⟩⟧ | ⟦⟨Reg⟩-⟨Reg⟩, ⟨Regs⟩⟧ ;

  sort Constant | ⟦#⟨Integer⟩⟧ | ⟦&⟨Label⟩⟧ ;

  // Helper concatenation/flattening of Instructions.
  sort Instructions | scheme ⟦ { ⟨Instructions⟩ } ⟨Instructions⟩ ⟧ ;
  ⟦ {} ⟨Instructions#⟩ ⟧ → # ;
  ⟦ {⟨Instruction#1⟩ ⟨Instructions#2⟩} ⟨Instructions#3⟩ ⟧
    → ⟦ ⟨Instruction#1⟩ {⟨Instructions#2⟩} ⟨Instructions#3⟩ ⟧ ;

  // Helper data structure for list of registers.
  sort Rs | NoRs | MoRs(Reg, Rs) | scheme AppendRs(Rs, Rs) ;
  AppendRs(NoRs, #Rs) → #Rs ;
  AppendRs(MoRs(#Rn, #Rs1), #Rs2) → MoRs(#Rn, AppendRs(#Rs1, #Rs2)) ;

  // Helper conversion from Regs syntax to register list.
  | scheme XRegs(Regs) ;
  XRegs(⟦⟨Reg#r⟩⟧) → MoRs(#r, NoRs) ;
  XRegs(⟦⟨Reg#r1⟩-⟨Reg#r2⟩⟧) → XRegs1(#r1, #r2) ;
  XRegs(⟦⟨Reg#r⟩, ⟨Regs#Rs⟩⟧) → MoRs(#r, XRegs(#Rs)) ;
  XRegs(⟦⟨Reg#r1⟩-⟨Reg#r2⟩, ⟨Regs#Rs⟩⟧) → AppendRs(XRegs1(#r1, #r2), XRegs(#Rs)) ;

  | scheme XRegs1(Reg, Reg) ;
  XRegs1(#r, #r) → MoRs(#r, NoRs) ;
  default XRegs1(#r1, #r2) → XRegs2(#r1, #r2) ;

  | scheme XRegs2(Reg, Reg) ;
  XRegs2(⟦R0⟧, #r2) → MoRs(⟦R0⟧, XRegs1(⟦R1⟧, #r2)) ;
  XRegs2(⟦R1⟧, #r2) → MoRs(⟦R1⟧, XRegs1(⟦R2⟧, #r2)) ;
  XRegs2(⟦R2⟧, #r2) → MoRs(⟦R2⟧, XRegs1(⟦R3⟧, #r2)) ;
  XRegs2(⟦R3⟧, #r2) → MoRs(⟦R3⟧, XRegs1(⟦R4⟧, #r2)) ;
  XRegs2(⟦R4⟧, #r2) → MoRs(⟦R4⟧, XRegs1(⟦R5⟧, #r2)) ;
  XRegs2(⟦R5⟧, #r2) → MoRs(⟦R5⟧, XRegs1(⟦R6⟧, #r2)) ;
  XRegs2(⟦R6⟧, #r2) → MoRs(⟦R6⟧, XRegs1(⟦R7⟧, #r2)) ;
  XRegs2(⟦R7⟧, #r2) → MoRs(⟦R7⟧, XRegs1(⟦R8⟧, #r2)) ;
  XRegs2(⟦R8⟧, #r2) → MoRs(⟦R8⟧, XRegs1(⟦R9⟧, #r2)) ;
  XRegs2(⟦R9⟧, #r2) → MoRs(⟦R9⟧, XRegs1(⟦R10⟧, #r2)) ;
  XRegs2(⟦R10⟧, #r2) → MoRs(⟦R10⟧, XRegs1(⟦R11⟧, #r2)) ;
  XRegs2(⟦R11⟧, #r2) → MoRs(⟦R11⟧, XRegs1(⟦R12⟧, #r2)) ;
  XRegs2(⟦R12⟧, #r2) → MoRs(⟦R12⟧, NoRs) ;
  XRegs1(⟦SP⟧, #r2) → error⟦MinARM32 error: Cannot use SP in Regs range.⟧ ;
  XRegs1(⟦LR⟧, #r2) → error⟦MinARM32 error: Cannot use LR in Regs range.⟧ ;
  XRegs1(⟦PC⟧, #r2) → error⟦MinARM32 error: Cannot use PC in Regs range.⟧ ;
  
  // Helpers to insert computed assembly constants.
  sort Constant | scheme Immediate(Computed) | scheme Reference(Computed) ;
  Immediate(#x) → ⟦#⟨INT#x⟩⟧ ;
  Reference(#id) → ⟦&⟨Label#id⟩⟧ ;

  sort Mem | scheme FrameAccess(Computed) ;
  FrameAccess(#x)
    → FrameAccess1(#x, ⟦ [R12, ⟨Constant Immediate(#x)⟩] ⟧, ⟦ [R12, -⟨Constant Immediate(⟦0-#x⟧)⟩] ⟧) ;
  | scheme FrameAccess1(Computed, Mem, Mem) ;
  FrameAccess1(#x, #pos, #neg) → FrameAccess2(⟦ #x ≥ 0 ? #pos : #neg ⟧) ;
  | scheme FrameAccess2(Computed) ;
  FrameAccess2(#mem) → #mem ;

  sort Instruction | scheme AddConstant(Reg, Reg, Computed) ;
  AddConstant(#Rd, #Rn, #x)
    → AddConstant1(#x,
		   ⟦ ADD ⟨Reg#Rd⟩, ⟨Reg#Rn⟩, ⟨Constant Immediate(#x)⟩ ⟧,
		   ⟦ SUB ⟨Reg#Rd⟩, ⟨Reg#Rn⟩, ⟨Constant Immediate(⟦0-#x⟧)⟩ ⟧) ;
  | scheme AddConstant1(Computed, Instruction, Instruction) ;
  AddConstant1(#x, #pos, #neg) → AddConstant2(⟦ #x ≥ 0 ? #pos : #neg ⟧) ;
  | scheme AddConstant2(Computed) ;
  AddConstant2(#add) → #add ;
  

  ////////////////////////////////////////////////////////////////////////
  // 3. COMPILER FROM MiniC TO MinARM32
  ////////////////////////////////////////////////////////////////////////

  // HACS doesn't like to compile with Computed sort
  // unless there exists a scheme that can generate Computed
  sort Computed | scheme Dummy ;
  Dummy → ⟦ 0 ⟧;

  // MAIN SCHEME

  sort Instructions  |  scheme Compile(Program) ;
  Compile(#1) → P2(P1(#1), #1) ;

  // PASS 1

  // Result sort for first pass, with join operation.
  sort After1 | Data1(Instructions, FT) | scheme Join1(After1, After1) ;
  Join1(Data1(#1, #ft1), Data1(#2, #ft2))
    → Data1(⟦ { ⟨Instructions#1⟩ } ⟨Instructions#2⟩ ⟧, AppendFT(#ft1, #ft2)) ;

  // Function to return type environment (list of pairs with append).
  sort FT | NoFT | MoFT(Identifier, Type, FT) | scheme AppendFT(FT, FT) ;
  AppendFT(NoFT, #ft2) → #ft2 ;
  AppendFT(MoFT(#id1, #T1, #ft1), #ft2) → MoFT(#id1, #T1, AppendFT(#ft1, #ft2)) ;

  // Pass 1 recursion.
  sort After1 | scheme P1(Program) ;
  P1(⟦⟨Declarations#Ds⟩⟧) → P1Ds(#Ds) ;

  sort After1 | scheme P1Ds(Declarations);  // Def. \ref{def:P}.
  P1Ds(⟦⟨Declaration#D⟩ ⟨Declarations#Ds⟩⟧) → Join1(D(#D), P1Ds(#Ds)) ;
  P1Ds(⟦⟧) → Data1(⟦⟧, NoFT) ;

  // \sem{D} scheme (Def. \ref{def:D}).
  
  sort After1 | scheme D(Declaration) ;
  D(⟦ function ⟨Type#T⟩ f ⟨ArgumentSignature#As⟩ { ⟨Statements#S⟩ } ⟧)
    → Data1(⟦⟧, MoFT(⟦f⟧, #T, NoFT)) ;

  // PASS 2
  
  sort Label | scheme Ident2Label(Identifier) ;
  Ident2Label(⟦⟨Identifier#i⟩⟧) → #i;
  

  // Pass 2 strategy: first load type environment $ρ$ then tail-call recursion.
  sort Instructions | scheme P2(After1, Program) ;
  P2(Data1(#1, #ft1), #P) → P2Load(#1, #ft1, #P) ;

  // Type environment ($ρ$) is split in two components (by used sorts).
  attribute ↓ft{Identifier : Type} ;	// map from function name to return type
  attribute ↓vt{Identifier : Local} ;	// map from local variable name to type\&location
  sort Local | RegLocal(Type, Reg) | FrameLocal(Type, Computed) ;  // type\&location

  // Other inherited attributes.
  attribute ↓return(Label) ;		// label of return code
  attribute ↓true(Label) ;		// label to jump for true result
  attribute ↓false(Label) ;		// label to jump for false result
  attribute ↓value(Reg) ;		// register for expression result
  attribute ↓offset(Computed) ;		// frame offset for first unused local
  attribute ↓unused(Rs) ;		// list of unused registers
  
  // Pass 2 Loader: extract type environment $ρ$ and emit pass 1 directives.
  sort Instructions | scheme P2Load(Instructions, FT, Program) ↓ft ↓vt ;
  P2Load(#is, MoFT(⟦f⟧, #T, #ft), #P) → P2Load(#is, #ft, #P) ↓ft{⟦f⟧ : #T} ;
  P2Load(#is, NoFT, #P) → ⟦ { ⟨Instructions#is⟩ } ⟨Instructions P(#P)⟩ ⟧ ;

  // Pass 2 recursion.
  sort Instructions | scheme P(Program) ↓ft ↓vt ;
  P(⟦ ⟨Declarations#Ds⟩ ⟧) → Ds(#Ds) ;

  sort Instructions | scheme Ds(Declarations) ↓ft ↓vt ;
  Ds(⟦ ⟨Declaration#D⟩ ⟨Declarations#Ds⟩ ⟧) → ⟦ { ⟨Instructions F(#D)⟩ } ⟨Instructions Ds(#Ds)⟩ ⟧ ;
  Ds(⟦⟧) → ⟦⟧ ;

  // \sem{F} scheme (Def. \ref{def:F}), with argument signature iteration helpers.
  
  sort Instructions | scheme F(Declaration) ↓ft ↓vt ;
  F(⟦ function ⟨Type#T⟩ f ⟨ArgumentSignature#AS⟩ { ⟨Statements#S⟩ } ⟧) → ⟦
	f	STMFD SP!, {R4-R12,LR}
		MOV R12, SP
		{ ⟨Instructions AS(#AS, XRegs(⟦R0-R3⟧), #S) ↓return(⟦L⟧)⟩ }
	L	MOV SP, R12
		LDMFD SP!, {R4-R12,PC}
  ⟧ ;
  
  sort Instructions | scheme AS(ArgumentSignature, Rs, Statements) ↓ft ↓vt ↓return ;
  AS(⟦ () ⟧, #Rs, #S) → S(#S) ↓offset(⟦0-4⟧) ↓unused(XRegs(⟦R4-R11⟧)) ;
  AS(⟦ ( ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ) ⟧, MoRs(#r, #Rs), #S)
    → AS2(#TIT, #Rs, #S) ↓vt{⟦a⟧ : RegLocal(#T, #r)} ;

  sort Instructions | scheme AS2(TypeIdentifierTail, Rs, Statements) ↓ft ↓vt ↓return ;
  AS2(⟦ ⟧, #Rs, #S) → S(#S) ↓offset(⟦0-4⟧) ↓unused(XRegs(⟦R4-R11⟧)) ;
  AS2(⟦ , ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ⟧, MoRs(#r, #Rs), #S)
    → AS2(#TIT, #Rs, #S) ↓vt{⟦a⟧ : RegLocal(#T, #r)} ;
  AS2(⟦ , ⟨Type#T⟩ a ⟨TypeIdentifierTail#TIT⟩ ⟧, NoRs, #S)
    → error⟦More than four arguments to function not allowed.⟧ ;

  // TODO: REMAINING CODE GENERATION.

  sort Instructions | scheme S(Statements) ↓ft ↓vt ↓return ↓unused ↓offset ;
  S(⟦ var ⟨Type#t⟩ ⟨Identifier#v⟩ ; ⟨Statements#ss⟩⟧) ↓offset(#off) ↓return(#ret) → ⟦
    SUB SP, SP, #4
    {⟨Instructions S(#ss) ↓vt{#v: FrameLocal(#t, #off)} ↓offset(⟦#off - 4⟧) ↓return(#ret)⟩}
  ⟧;
  S(⟦ ⟨Statement#s⟩ ⟨Statements#ss⟩⟧) ↓unused(#unused) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret) → ⟦
    {⟨Instructions S1(#s) ↓unused(#unused) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret)⟩}
    ⟨Instructions S(#ss) ↓vt{:#vt} ↓ft{:#ft} ↓unused(#unused) ↓return(#ret)⟩
  ⟧ ;
  // {⟨Instructions S(⟦⟨Statement#s⟩⟧) ↓vt{:#vt} ↓ft{:#ft} ↓unused(MoRs(#r, #Rs))⟩}
  S(⟦⟧) → ⟦⟧ ;
  default S(#) → error ⟦Scode gen error.⟧ ;

  sort Instructions | scheme S1(Statement) ↓ft ↓vt ↓return ↓unused ↓offset;
  S1(⟦ return ⟨Expression#e⟩ ;⟧) ↓unused(MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret) → ⟦
    {⟨Instructions Ecode(#e, #r, MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft}⟩}
    MOV R0, ⟨Reg #r⟩
    B ⟨Label#ret⟩
  ⟧;
  S1(⟦ ⟨Identifier#v⟩ = ⟨Expression#e⟩ ;⟧) ↓unused(MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions Ecode(#e, #r, MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions SAssign(#v, #r) ↓vt{:#vt} ↓ft{:#ft}⟩}
  ⟧ ;
  S1(⟦ ⟨Expression#t⟩ = ⟨Expression#e⟩ ;⟧) ↓unused(MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions Ecode(#t, #r, MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions Ecode(#e, ⟦R11⟧, #Rs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    STR R11, [⟨Reg#r⟩, #0]
  ⟧ ;
  S1(⟦ if ( ⟨Expression#e⟩ ) ⟨Statement#s⟩ ⟧) ↓unused(MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret) ↓offset(#off) → ⟦
    {⟨Instructions Ecode(#e, #r, MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft}⟩}
    CMP ⟨Reg#r⟩, #1
    BNE done
    {⟨Instructions S1(#s) ↓vt{:#vt} ↓ft{:#ft} ↓unused(MoRs(#r, #Rs)) ↓return(#ret)⟩}
  done
  ⟧ ;
  S1(⟦ if ( ⟨Expression#e⟩ ) ⟨Statement#s1⟩ else ⟨Statement#s2⟩⟧) ↓unused(MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret) ↓offset(#off) → ⟦
    {⟨Instructions Ecode(#e, #r, MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret)⟩}
    CMP ⟨Reg#r⟩, #1
    BNE els
    {⟨Instructions S1(#s1) ↓vt{:#vt} ↓ft{:#ft} ↓unused(MoRs(#r, #Rs)) ↓return(#ret) ↓offset(#off)⟩}
    B done
  els
    {⟨Instructions S1(#s2) ↓vt{:#vt} ↓ft{:#ft} ↓unused(MoRs(#r, #Rs)) ↓return(#ret) ↓offset(#off)⟩}
  done
  ⟧ ;
  S1(⟦ while ( ⟨Expression#e⟩ ) ⟨Statement#s1⟩ ⟧) ↓unused(MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret) ↓offset(#off) → ⟦
  repeat
    {⟨Instructions Ecode(#e, #r, MoRs(#r, #Rs)) ↓vt{:#vt} ↓ft{:#ft}⟩}
    CMP ⟨Reg#r⟩, #0
    BEQ done
    {⟨Instructions S1(#s1) ↓vt{:#vt} ↓ft{:#ft} ↓unused(MoRs(#r, #Rs)) ↓return(#ret) ↓offset(#off)⟩}
    B repeat
  done
  ⟧ ;
  S1(⟦{⟨Statements#ss⟩}⟧) ↓unused(#unused) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret) ↓offset(#off)→ 
    S(#ss) ↓unused(#unused) ↓vt{:#vt} ↓ft{:#ft} ↓return(#ret) ↓offset(#off);

  | scheme SAssign(Identifier, Reg) ↓vt ;
  SAssign(#v, #r) ↓vt{#v: FrameLocal(#t, #off)} → ⟦
    STR ⟨Reg#r⟩, ⟨Mem FrameAccess(#off)⟩
  ⟧ ;
  SAssign(#v, #r) ↓vt{#v: RegLocal(#t, #rr)} → ⟦
    MOV ⟨Reg#rr⟩, ⟨Reg#r⟩
  ⟧ ;
  SAssign(#v, #r) ↓vt{¬#v} → error ⟦no such variable.⟧ ;

  // scheme RegAlloc2(Expression, Expression, Rs, Reg, Expression);
  // Alloc registers (for operands) and export code for binary operator expression
  // - Operand 1's Exp
  // - Operand 2's Exp
  // - available registers
  // - result register
  // - Expression

  | scheme RegAlloc2(Expression, Expression, Rs, Reg, Expression) ↓vt ↓ft;
  RegAlloc2(#1, #2, MoRs(#r1, MoRs(#r2, #Rs2)), #r, #exp) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions Ecode(#1, #r1, MoRs(#r1, MoRs(#r2, #Rs2))) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions Ecode(#2, #r2, MoRs(#r2, #Rs2)) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions Ecode2(#exp, #r, #r1, #r2)⟩}
  ⟧ ;
  RegAlloc2(#1, #2, MoRs(#r1, NoRs), #r, #exp) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions Ecode(#1, #r1, MoRs(#r1, NoRs)) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions Ecode(#2, ⟦R11⟧, NoRs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions Ecode2(#exp, ⟦R11⟧, #r1, ⟦R11⟧)⟩}
  ⟧ ;
  RegAlloc2(#1, #2, NoRs, #r, #exp) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {STMFD SP!, {R4}}
    {⟨Instructions Ecode(#1, ⟦R11⟧, NoRs) ↓vt{:#vt} ↓ft{:#ft} ⟩}
    {⟨Instructions Ecode(#2, ⟦R4⟧, NoRs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions Ecode2(#exp, ⟦R11⟧, ⟦R4⟧, ⟦R11⟧)⟩}
    {LDMFD SP!, {R4}}
  ⟧ ;

  // scheme RegAlloc1(Expression, Expression, Rs, Reg, Expression);
  // Alloc register (for operand) and export code for unary operator expression
  // - Operand's Exp
  // - available registers
  // - result register
  // - Expression

  | scheme RegAlloc1(Expression, Rs, Reg, Expression) ↓vt ↓ft;
  RegAlloc1(#1, #Rs, #r, #exp) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions Ecode(#1, ⟦R11⟧, #Rs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions Ecode1(#exp, #r, ⟦R11⟧)⟩}
  ⟧ ;

  | scheme RegAllocArg(Expression, Rs) ↓vt ↓ft;
  RegAllocArg(#1, #Rs) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions Ecode(#1, ⟦R11⟧, #Rs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    STMFD SP!, {R11}
  ⟧ ;

  | scheme Ecode2(Expression, Reg, Reg, Reg) ;
  Ecode2(⟦⟨Expression#1⟩ + ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦ADD ⟨Reg#r⟩, ⟨Reg#r1⟩, ⟨Reg#r2⟩⟧ ;
  Ecode2(⟦⟨Expression#1⟩ - ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦SUB ⟨Reg#r⟩, ⟨Reg#r1⟩, ⟨Reg#r2⟩⟧ ;
  Ecode2(⟦⟨Expression#1⟩ * ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦MUL ⟨Reg#r⟩, ⟨Reg#r1⟩, ⟨Reg#r2⟩⟧ ;
  Ecode2(⟦⟨Expression#1⟩ > ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦
    CMP ⟨Reg#r1⟩, ⟨Reg#r2⟩
    BGT true
    MOV ⟨Reg#r⟩, #0
    B done
  true
    MOV ⟨Reg#r⟩, #1
  done
  ⟧ ;
  Ecode2(⟦⟨Expression#1⟩ < ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦
    CMP ⟨Reg#r1⟩, ⟨Reg#r2⟩
    BLT true
    MOV ⟨Reg#r⟩, #0
    B done
  true
    MOV ⟨Reg#r⟩, #1
  done
  ⟧ ;
  Ecode2(⟦⟨Expression#1⟩ >= ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦
    CMP ⟨Reg#r1⟩, ⟨Reg#r2⟩
    BGE true
    MOV ⟨Reg#r⟩, #0
    B done
  true
    MOV ⟨Reg#r⟩, #1
  done
  ⟧ ;
  Ecode2(⟦⟨Expression#1⟩ <= ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦
    CMP ⟨Reg#r1⟩, ⟨Reg#r2⟩
    BLE true
    MOV ⟨Reg#r⟩, #0
    B done
  true
    MOV ⟨Reg#r⟩, #1
  done
  ⟧ ;
  Ecode2(⟦⟨Expression#1⟩ == ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦
    CMP ⟨Reg#r1⟩, ⟨Reg#r2⟩
    BEQ true
    MOV ⟨Reg#r⟩, #0
    B done
  true
    MOV ⟨Reg#r⟩, #1
  done
  ⟧ ;
  Ecode2(⟦⟨Expression#1⟩ != ⟨Expression#2⟩⟧, #r, #r1, #r2) → ⟦
    CMP ⟨Reg#r1⟩, ⟨Reg#r2⟩
    BNE true
    MOV ⟨Reg#r⟩, #0
    B done
  true
    MOV ⟨Reg#r⟩, #1
  done
  ⟧ ;

  | scheme Ecode1(Expression, Reg, Reg) ;
  Ecode1(⟦- ⟨Expression#1⟩⟧, #r, #r1) → ⟦RSB ⟨Reg#r⟩, ⟨Reg#r1⟩, # 0⟧ ;

  Ecode1(⟦* ⟨Expression#1 ↑t(⟦* char⟧)⟩⟧, #r, #r1) → ⟦
    LDR ⟨Reg#r⟩, [⟨Reg#r1⟩, # 0]
    AND ⟨Reg#r⟩, ⟨Reg#r⟩, # 255
  ⟧ ;
  Ecode1(⟦* ⟨Expression#1 ↑t(#)⟩⟧, #r, #r1) → ⟦
    LDR ⟨Reg#r⟩, [⟨Reg#r1⟩, # 0]
  ⟧ ;
  Ecode1(⟦* ⟨Expression#1⟩⟧, #r, #r1) → ⟦LDR ⟨Reg#r⟩, [⟨Reg#r1⟩, # 0]⟧ ;

  | scheme ArgsCode(ExpressionList, Rs, Rs) ↓vt ↓ft;
  ArgsCode(⟦ ⟧ , #, #) → ⟦⟧;
  ArgsCode(⟦⟨Expression#e⟩ ⟨ExpressionListTail#el⟩⟧, MoRs(#r, #Rs), #unused) ↓vt{:#vt} ↓ft{:#ft} →
    ⟦
      {⟨Instructions RegAllocArg(#e, #unused) ↓vt{:#vt} ↓ft{:#ft}⟩}
      {⟨Instructions ArgsCode2(#el, #Rs, #unused) ↓vt{:#vt} ↓ft{:#ft}⟩}
      LDMFD SP!, {⟨Reg#r⟩}
    ⟧ ;

  | scheme ArgsCode2(ExpressionListTail, Rs, Rs) ↓vt ↓ft;
  ArgsCode2(⟦ ⟧ , #, #unused) → ⟦⟧;
  ArgsCode2(⟦ , ⟨Expression#e⟩ ⟨ExpressionListTail#el⟩ ⟧ , MoRs(#r, #Rs), #unused) ↓vt{:#vt} ↓ft{:#ft} →
    ⟦
      {⟨Instructions RegAllocArg(#e, #unused) ↓vt{:#vt} ↓ft{:#ft}⟩}
      {⟨Instructions ArgsCode2(#el, #Rs, #unused) ↓vt{:#vt} ↓ft{:#ft}⟩}
      LDMFD SP!, {⟨Reg#r⟩}
    ⟧ ;
  ArgsCode2(⟦ , ⟨Expression#e⟩ ⟨ExpressionListTail#el⟩ ⟧ , NoRs, #unused) ↓vt{:#vt} ↓ft{:#ft} →
    error ⟦not support more than 4 arguments⟧;
  default ArgsCode2(#, #, #) → error ⟦ArgsCode2 error⟧;

  // FunCode(Expression, ExpressionList, Reg, Rs)
  // * generate function call code
  // - Caller Expression
  // - Arguments list
  // - Result Register
  // - Available Registers
  | scheme FunCode(Expression, ExpressionList, Reg, Rs) ↓vt ↓ft;
  FunCode(⟦⟨Identifier#v⟩⟧, ⟦ ⟧, #r, #unused) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {STMFD SP!, {R0}} 
    {BL ⟨Label Ident2Label(#v)⟩} 
    {MOV ⟨Reg#r⟩, R0}
    LDMFD SP!, {R0}
  ⟧ ;
  FunCode(⟦⟨Identifier#v⟩⟧, #args, #r, #unused) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {STMFD SP!, {R0-R3}} 
    {⟨Instructions ArgsCode(#args, XRegs(⟦R0-R3⟧), #unused) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {BL ⟨Label Ident2Label(#v)⟩} 
    {MOV ⟨Reg#r⟩, R0}
    LDMFD SP!, {R0-R3}
  ⟧ ;
  FunCode(⟦⟨Expression#e⟩⟧, ⟦ ⟧, #r, #unused) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {STMFD SP!, {R0}} 
    {⟨Instructions Ecode(#e, #r, #unused) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {MOV LR, PC}
    {MOV PC, ⟨Reg#r⟩}
    {MOV ⟨Reg#r⟩, R0}
    LDMFD SP!, {R0}
  ⟧ ;
  FunCode(⟦⟨Expression#e⟩⟧, #args, #r, #unused) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {STMFD SP!, {R0-R3}}
    {⟨Instructions ArgsCode(#args, XRegs(⟦R0-R3⟧), #unused) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {⟨Instructions Ecode(#e, #r, #unused) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {MOV LR, PC}
    {MOV PC, ⟨Reg#r⟩}
    {MOV ⟨Reg#r⟩, R0}
    LDMFD SP!, {R0-R3}
  ⟧ ;

  sort Instructions | scheme Ecode(Expression, Reg, Rs) ↓vt ↓ft;
  Ecode(#e, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → EcodeC(EcodeTA(#e) ↓vt{:#vt} ↓ft{:#ft}, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft};
  {
    | scheme EcodeC(Expression, Reg, Rs) ↓vt ↓ft;
    EcodeC(#e, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → EcodeB(#e, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft};
  }

  attribute ↑t(Type) ;

  sort Type | scheme TCDeRef(Type);
  TCDeRef(⟦* ⟨Type#⟩⟧) → #;

  sort Expression | ↑t;
  ⟦⟨Integer#⟩⟧↑t(⟦int⟧);
  ⟦⟨String#⟩⟧↑t(⟦* char⟧);
  ⟦null(⟨Type#⟩)⟧↑t(#);
  ⟦sizeof(⟨Type#⟩)⟧↑t(⟦int⟧);
  ⟦* ⟨Expression# ↑t(#t)⟩⟧↑t(TCDeRef(#t));
  ⟦& ⟨Expression# ↑t(#t)⟩⟧↑t(⟦* ⟨Type#t⟩⟧);

  sort Expression | scheme EcodeTA(Expression) ↓vt ↓ft;
  EcodeTA(⟦⟨Identifier#v⟩⟧) ↓vt{#v: RegLocal(#t, #r1)} →⟦⟨Identifier#v⟩⟧↑t(#t);
  EcodeTA(⟦⟨Identifier#v⟩⟧) ↓vt{#v: FrameLocal(#t, #off)} →⟦⟨Identifier#v⟩⟧↑t(#t);
  EcodeTA(⟦⟨String#s⟩⟧) →  ⟦⟨String#s⟩⟧↑t(⟦* char⟧);
  EcodeTA(⟦⟨Integer#i⟩⟧) → ⟦⟨Integer#i⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦* ⟨Expression#⟩⟧) → EcodeTAUna(⟦* ⟨Expression EcodeTA(#)⟩⟧);
  {
    | scheme EcodeTAUna(Expression);
    EcodeTAUna(⟦* ⟨Expression# ↑t(#t)⟩⟧) → ⟦* ⟨Expression#⟩⟧↑t(TCDeRef(#t));
  }
  EcodeTA(⟦- ⟨Expression#⟩⟧) → ⟦- ⟨Expression#⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦! ⟨Expression#⟩⟧) → ⟦! ⟨Expression#⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦⟨Expression#1⟩ + ⟨Expression#2⟩⟧) → EcodeTABin(⟦⟨Expression EcodeTA(#1)⟩ + ⟨Expression EcodeTA(#2)⟩⟧);
  EcodeTA(⟦⟨Expression#1⟩ - ⟨Expression#2⟩⟧) → EcodeTABin(⟦⟨Expression EcodeTA(#1)⟩ - ⟨Expression EcodeTA(#2)⟩⟧);
  EcodeTA(⟦⟨Expression#1⟩ * ⟨Expression#2⟩⟧) → EcodeTABin(⟦⟨Expression EcodeTA(#1)⟩ * ⟨Expression EcodeTA(#2)⟩⟧);
  {
    | scheme EcodeTABin(Expression);
    EcodeTABin(⟦⟨Expression#1 ↑t(#t1)⟩ + ⟨Expression#2 ↑t(#t2)⟩⟧) → ⟦⟨Expression#1⟩ + ⟨Expression#2⟩⟧↑t(#t1);
    EcodeTABin(⟦⟨Expression#1 ↑t(#t1)⟩ - ⟨Expression#2 ↑t(#t2)⟩⟧) → ⟦⟨Expression#1⟩ - ⟨Expression#2⟩⟧↑t(#t1);
    EcodeTABin(⟦⟨Expression#1 ↑t(#t1)⟩ * ⟨Expression#2 ↑t(#t2)⟩⟧) → ⟦⟨Expression#1⟩ * ⟨Expression#2⟩⟧↑t(#t1);
  }
  EcodeTA(⟦⟨Identifier#v⟩ ( )⟧) ↓ft{#v: #t} → ⟦⟨Identifier#v⟩ ( )⟧↑t(#t);
  EcodeTA(⟦⟨Identifier#v⟩ ( ⟨ExpressionList#el⟩ )⟧) ↓ft{#v: #t} → ⟦⟨Identifier#v⟩ ( ⟨ExpressionList#el⟩ )⟧↑t(#t);

  EcodeTA(⟦⟨Expression#1⟩ || ⟨Expression#2⟩⟧) → ⟦⟨Expression#1⟩ || ⟨Expression#2⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦⟨Expression#1⟩ && ⟨Expression#2⟩⟧) → ⟦⟨Expression#1⟩ && ⟨Expression#2⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦⟨Expression#1⟩ == ⟨Expression#2⟩⟧) → ⟦⟨Expression#1⟩ == ⟨Expression#2⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦⟨Expression#1⟩ != ⟨Expression#2⟩⟧) → ⟦⟨Expression#1⟩ != ⟨Expression#2⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦⟨Expression#1⟩ >= ⟨Expression#2⟩⟧) → ⟦⟨Expression#1⟩ >= ⟨Expression#2⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦⟨Expression#1⟩ <= ⟨Expression#2⟩⟧) → ⟦⟨Expression#1⟩ <= ⟨Expression#2⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦⟨Expression#1⟩ > ⟨Expression#2⟩⟧) → ⟦⟨Expression#1⟩ > ⟨Expression#2⟩⟧↑t(⟦int⟧);
  EcodeTA(⟦⟨Expression#1⟩ < ⟨Expression#2⟩⟧) → ⟦⟨Expression#1⟩ < ⟨Expression#2⟩⟧↑t(⟦int⟧);
  

  sort Instructions | scheme EcodeB(Expression, Reg, Rs) ↓vt ↓ft;
  EcodeB(⟦⟨Identifier#v⟩⟧, #r, #Rs) ↓vt{#v: RegLocal(#t, #r1)} → ⟦MOV ⟨Reg#r⟩, ⟨Reg#r1⟩⟧;
  EcodeB(⟦⟨Identifier#v⟩⟧, #r, #Rs) ↓vt{#v: FrameLocal(#t, #off)} → ⟦LDR ⟨Reg#r⟩, ⟨Mem FrameAccess(#off)⟩⟧;
  EcodeB(⟦⟨String#s⟩⟧, #r, #Rs) → EcodeStr(#s, #r, ⟦str⟧);
  EcodeB(⟦⟨Integer#i⟩⟧, #r, #Rs) → EcodeInt(#i, #r, ⟦i⟧);

  | scheme EcodeInt(Integer, Reg, Label) ;
  EcodeInt(#i, #r, #l) → ⟦
  ⟨Label#l⟩ DCI ⟨Integer#i⟩
      MOV ⟨Reg#r⟩, &⟨Label#l⟩
  ⟧;

  | scheme EcodeStr(String, Reg, Label) ;
  EcodeStr(#s, #r, #l) → ⟦
  ⟨Label#l⟩ DCS ⟨String#s⟩
      MOV ⟨Reg#r⟩, &⟨Label#l⟩
  ⟧;

  // Binary Operators
  EcodeB(⟦⟨Expression#1⟩ + ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ + ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦⟨Expression#1⟩ - ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ - ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦⟨Expression#1⟩ * ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ * ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦⟨Expression#1⟩ > ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ > ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦⟨Expression#1⟩ < ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ < ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦⟨Expression#1⟩ >= ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ >= ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦⟨Expression#1⟩ <= ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ <= ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦⟨Expression#1⟩ == ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ == ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦⟨Expression#1⟩ != ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc2(#1, #2, #Rs, #r, ⟦⟨Expression#1⟩ != ⟨Expression#2⟩⟧) ↓vt{:#vt} ↓ft{:#ft};

  EcodeB(⟦⟨Expression#1⟩ || ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions EcodeB(#1, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    CMP ⟨Reg#r⟩, #0
    BNE true
    {⟨Instructions EcodeB(#2, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    CMP ⟨Reg#r⟩, #0
    BNE true
    MOV ⟨Reg#r⟩, #0
    B done
  true MOV ⟨Reg#r⟩, #1
  done
  ⟧ ;

  EcodeB(⟦⟨Expression#1⟩ && ⟨Expression#2⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions EcodeB(#1, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {CMP ⟨Reg#r⟩, #0}
    {BEQ false}
    {⟨Instructions EcodeB(#2, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    {CMP ⟨Reg#r⟩, #0}
    {BEQ false}
    {MOV ⟨Reg#r⟩, #0}
    {B done}
  false MOV ⟨Reg#r⟩, #0
  done
  ⟧ ;

  EcodeB(⟦! ⟨Expression#1⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → ⟦
    {⟨Instructions EcodeB(#1, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft}⟩}
    CMP ⟨Reg#r⟩, #0
    BEQ true
    MOV ⟨Reg#r⟩, #0
    B done
  true MOV ⟨Reg#r⟩, #1
  done
  ⟧ ;
  
  // Unary Operators
  EcodeB(⟦- ⟨Expression#1⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc1(#1, #Rs, #r, ⟦- ⟨Expression#1⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦* ⟨Expression#1⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    RegAlloc1(#1, #Rs, #r, ⟦* ⟨Expression#1⟩⟧) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦& * ⟨Expression#1⟩⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} → 
    EcodeB(#1, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦& ⟨Identifier#v⟩⟧, #r, #Rs) ↓vt{#v: FrameLocal(#t, #off)} → ⟦
    {MOV ⟨Reg#r⟩, R12}
    ⟨Instruction AddConstant(#r, #r, #off)⟩
  ⟧ ;
  EcodeB(⟦& ⟨Expression#e⟩⟧, #r, #Rs) → error ⟦ illegal address fetching. ⟧ ;

  // function call
  
  EcodeB(⟦ ⟨Expression#caller⟩ ( ⟨ExpressionList#args⟩ ) ⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} →
    FunCode(#caller, #args, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft};
  EcodeB(⟦ ⟨Expression#caller⟩ ( ) ⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft} →
    FunCode(#caller, ⟦ ⟧, #r, #Rs) ↓vt{:#vt} ↓ft{:#ft};

  default EcodeB(#, #, #) → error ⟦Ecode gen error.⟧ ;

}